import { HTMLDocument } from "deno_dom/deno-dom-wasm-noinit.ts";
export {};

declare global {
  type Ref<T> = { current: T };
  type PromiseOr<R> = Promise<R> | R;
  // deno-lint-ignore no-explicit-any
  type dynamic = any;

  const document: HTMLDocument;
}
