enum ItemType {
  Movie = "movie",
  Series = "tv",
  Episode = "episode",
}

export default ItemType;
