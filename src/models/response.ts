// export type ResponseBody<T = unknown> =
//   | { payload: T }
//   | {
//     error: {
//       message: string;
//       status?: number;
//     };
//   };

export class ApiResponse {
  static success<T>(body: T) {
    return {
      payload: body,
    };
  }
  static failure(message: string, status = 400) {
    return {
      message,
      status,
    };
  }
}
