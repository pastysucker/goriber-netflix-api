// ISO 639-1 codes
export enum LanguageCode {
  Arabic = "ar",
  Bengali = "bn",
  English = "en",
  French = "fr",
  German = "de",
  Hindi = "hi",
  Persian = "fa",
  Russian = "ru",
  Spanish = "es",
  Turkish = "tr",
  Urdu = "ur",
}

export namespace LanguageCode {
  type LanguageNames = Exclude<keyof typeof LanguageCode, "searchString">;
  export function searchString(searchKey: string): LanguageCode | undefined {
    const newKey = (Object.keys(LanguageCode) as LanguageNames[]).find((key) =>
      searchKey.includes(key as LanguageNames)
    );
    if (newKey != null) {
      return LanguageCode[newKey];
    }
  }
}

export type ImageUris = {
  primary?: string;
  backdrop?: string;
  thumb?: string;
  logo?: string;
  banner?: string;
};

export interface MediaSource {
  streamUri: string;
  bitrate?: number;
  fileSize?: number;
  fileName: string;
  displayName: string;
  mimeType?: string;
  language?: LanguageCode;
}

export interface SearchResult {
  id: string;
  name: string;
  isMovie: boolean;
  year?: number;
  imageUris?: ImageUris;
  adult: boolean;
}

export interface PersonResult {
  id: string;
  name: string;
  gender: "male" | "female" | "non-binary" | "unknown";
  imageUris: ImageUris;
  department: string;
}

export interface SubtitleResult {
  url: string;
  language: string;
}

export interface CriticRatings {
  tmdb?: number;
  rottenTomatoes?: number;
}

export interface ExternalIds {
  tmdb?: number;
  imdb?: string;
  tvdb?: number;
}

export interface Cast {
  id: string;
  name: string;
  role?: string;
  imageUris?: ImageUris;
}

export interface Network {
  id: string;
  name: string;
  imageUris?: ImageUris;
}

export type Studio = string;
