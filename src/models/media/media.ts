import { Cast, ExternalIds, ImageUris } from "./common.ts";

export interface Media {
  id: string;
  title?: string;
  year?: number;
  externalIds?: ExternalIds;
  genres?: string[];
  ageRating?: string;
  tagline?: string;
  synopsis?: string;
  imageUris?: ImageUris;
  cast?: Cast[];
  adult: boolean;
}
