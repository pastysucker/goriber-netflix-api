import { Media } from "./media.ts";
import { CriticRatings, ExternalIds, ImageUris, Network } from "./common.ts";

export interface Series extends Media {
  averageRuntime?: number;
  hasEnded?: boolean;
  lastAired?: Date;
  criticRatings?: CriticRatings;
  childCount: number;
  networks?: Network[];
}

export interface Season {
  id: string;
  seriesId: string;
  index: number;
  name: string;
  childCount: number;
  imageUris?: ImageUris;
}

export interface Episode {
  id: string;
  seriesId: string;
  seasonIndex: number;
  index: number;
  name: string;
  directors?: string[];
  synopsis?: string;
  runtime?: number;
  airDate?: Date;
  imageUris?: ImageUris;
  externalIds?: ExternalIds;
  // mediaSources?: MediaSource[];
}
