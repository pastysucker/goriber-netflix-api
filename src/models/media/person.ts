import { ExternalIds, ImageUris } from "./common.ts";

export interface Person {
  id: string;
  name: string;
  biography: string;
  gender: "male" | "female" | "non-binary" | "unknown";
  birthplace?: string;
  imageUris: ImageUris;
  externalIds: ExternalIds;
  department: string;
}
