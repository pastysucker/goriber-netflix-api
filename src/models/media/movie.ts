import { Media } from "./media.ts";
import { CriticRatings, Studio } from "./common.ts";

export interface Movie extends Media {
  directors?: string[];
  runtime: number; // in minutes
  studios?: Studio[];
  criticRatings: CriticRatings;
}
