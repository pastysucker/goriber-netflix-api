import { Parser } from "./parser.ts";
import { SearchResult } from "./media/common.ts";
import { Movie } from "./media/movie.ts";
import { Episode, Season, Series } from "./media/series.ts";
import ItemType from "types/itemType.ts";

export interface Engine {
  readonly parser: Parser;
  searchItems(
    query: string,
    itemType: ItemType,
  ): Promise<SearchResult[]>;
  searchItems(
    query: string,
    itemType: ItemType,
    limit: number,
    skip: number,
  ): Promise<SearchResult[]>;

  discoverItems(
    itemType: ItemType,
    args: {
      networks?: string[];
      genres?: string[];
      people?: string[];
    },
  ): Promise<SearchResult[]>;

  getSimilarItems(
    itemId: string,
    itemType: ItemType,
  ): Promise<SearchResult[]>;

  getMovie(id: string): Promise<Movie>;

  getSeries(id: string): Promise<Series>;

  getSeasons(seriesId: string): Promise<Season[]>;

  getSeason(seriesId: string, seasonIndex: number): Promise<Season>;

  getEpisodes(
    seriesId: string,
    seasonIndex: number,
  ): Promise<Episode[]>;

  getEpisode(
    seriesId: string,
    seasonIndex: number,
    episodeIndex: number,
  ): Promise<Episode>;

  // abstract getPlaybackInfo(id: string): Promise<MediaSource[]>;
}
