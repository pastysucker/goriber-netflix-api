/// <reference types="../types/types.d.ts" />
import ItemType from "types/itemType.ts";
import { Movie } from "./media/movie.ts";
import { Episode, Season, Series } from "./media/series.ts";
import { SearchResult } from "./media/common.ts";

export interface Parser {
  parseMovie(json: dynamic): Movie;
  parseSeries(json: dynamic): Series;
  parseResult(json: dynamic, itemType: ItemType): SearchResult;
  parseSeason(json: dynamic, seriesId: string): Season;
  parseEpisode(
    json: dynamic,
    seriesId?: string,
    seasonIndex?: number,
  ): Episode;
}
