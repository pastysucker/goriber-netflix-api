import {
  Edition,
  ParsedFilename,
  Resolution,
  Source,
} from "video-filename-parser";

export interface DirObject {
  name: string;
  path: URL;
  isFile: boolean;
  lastModified?: Date;
  fileSize?: number;
  mimeType?: string;
  meta?: ParsedFilename | ParsedFolder /*& {
    images?: Image[];
  };*/;
}

export interface ParsedFolder {
  title?: string;
  year?: string;
  seasons?: number[];
  resolution?: Resolution;
  dubbing?: string;
  sources?: Source[];
  edition?: Edition;
  isFolder: true;
}

// abstract class
export interface NodeTreeBuilder {
  readonly name: string;
  getNodes: (arg: URL, isSeries: boolean) => Promise<DirObject[]>;
}
