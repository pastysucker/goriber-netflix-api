import { MediaSource } from "./media/common.ts";

export interface ScraperArgs {
  id?: string;
  imdbId?: string;
  tvdbId?: number;
  title?: string;
  year?: number;
  seasonIndex?: number;
  episodeIndex?: number;
  isSeries: boolean;
  isNsfw: boolean;
}

export abstract class Scraper {
  abstract readonly name: string;
  abstract readonly nsfw: boolean;

  abstract scrapeSources(args: ScraperArgs): Promise<MediaSource[]>;
}
