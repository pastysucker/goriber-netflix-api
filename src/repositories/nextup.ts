import { query } from "shared/db/sqlite.ts";
import { createHttpError } from "oak/deps.ts";
import { tmdbEngine } from "shared/inits.ts";

export function getAll(userId: string) {
  const results = query<
    [string, string, string | null, string, number, number, string]
  >`
    SELECT
      series.id,
      series.name,
      episode.i_backdrop,
      episode.name,
      nextup.season_index,
      nextup.episode_index,
      nextup.created
    FROM nextup
    LEFT JOIN media AS series
      ON series.id = nextup.series_id
    LEFT JOIN media AS episode
      ON episode.id = nextup.episode_id
    WHERE nextup.user_id = ${userId}
    `;

  const formatted = results.map((item) => {
    const [
      seriesId,
      seriesName,
      backdrop,
      episodeName,
      seasonIndex,
      episodeIndex,
      created,
    ] = item;
    return {
      seriesId,
      seriesName,
      episodeName,
      seasonIndex,
      episodeIndex,
      created,
      imageUris: {
        backdrop,
      },
    };
  });
  return formatted;
}

export function getOne(seriesId: string, userId: string) {
  const [result] = query<
    [string, string, number, number, string, number, string]
  >`
      SELECT
        series.id,
        series.name,
        episode.i_backdrop,
        nextup.season_index,
        nextup.episode_index,
        episode.name,
        nextup.created
      FROM nextup
      LEFT JOIN media AS series
        ON series.id = nextup.series_id
      LEFT JOIN media AS episode
        ON episode.id = nextup.episode_id
      WHERE nextup.series_id = ${seriesId}
      AND nextup.user_id = ${userId}
      `;

  if (result == null) {
    throw createHttpError(404, "No next up entry found");
  }
  const [
    seriesId_,
    seriesName,
    backdrop,
    seasonIndex,
    episodeIndex,
    episodeName,
    created,
  ] = result;
  return {
    seriesId: seriesId_,
    seriesName,
    episodeName,
    seasonIndex,
    episodeIndex,
    created,
    imageUris: {
      backdrop,
    },
  };
}

export async function create(
  seriesId: string,
  seasonIndex: number,
  episodeIndex: number,
  userId: number,
) {
  // delete expired
  query`DELETE FROM nextup WHERE (created <= datetime('now', '-14 days'))`;

  const series = await tmdbEngine.getSeries(seriesId);
  const seasons = await tmdbEngine.getSeasons(seriesId);

  const currentSeason = seasons[seasonIndex - 1]; // 4 / 8
  const numberOfSeasons = seasons.length; // 8
  const episodesInSeason = currentSeason.childCount; // 20
  // const totalEpisodesInSeries = seasons.reduce(
  //   (prev, next) => prev + next.childCount,
  //   0,
  // );

  try {
    const { nextEpisode, nextSeason } = getNextEpisode({
      currentEpisode: episodeIndex,
      currentSeason: seasonIndex,
      episodesInSeason,
      totalSeasons: numberOfSeasons,
    });
    const episode = await tmdbEngine.getEpisode(
      seriesId,
      nextSeason,
      nextEpisode,
    );

    query`
        INSERT OR IGNORE INTO media (id, name, media_type, year, i_primary, i_backdrop, i_logo, i_thumb, i_banner)
        values (
          ${seriesId},
          ${series.title},
          'series',
          ${series.year},
          ${series.imageUris?.primary ?? null},
          ${series.imageUris?.backdrop ?? null},
          ${series.imageUris?.logo ?? null},
          ${series.imageUris?.thumb ?? null},
          ${series.imageUris?.banner ?? null}
        )
      `;

    query`
        INSERT OR IGNORE INTO media (id, name, media_type, year, i_primary, i_backdrop, i_logo, i_thumb, i_banner)
        values (
          ${episode.id},
          ${episode.name},
          'episode',
          ${episode.airDate?.getFullYear()},
          ${episode.imageUris?.primary ?? null},
          ${episode.imageUris?.backdrop ?? null},
          ${episode.imageUris?.logo ?? null},
          ${episode.imageUris?.thumb ?? null},
          ${episode.imageUris?.banner ?? null}
        )
      `;

    query`
        INSERT OR REPLACE INTO nextup
          (series_id, season_index, episode_index, episode_id, user_id)
        VALUES (
          ${series.id},
          ${nextSeason},
          ${nextEpisode},
          ${episode.id},
          ${userId}
        )
      `;

    // Purge media rows that have no child references
    query`DELETE FROM media WHERE id = ${seriesId} AND NOT EXISTS (SELECT * FROM nextup WHERE series_id = ${seriesId})`;
    query`DELETE FROM media WHERE id = ${episode.id} AND NOT EXISTS (SELECT * FROM nextup WHERE episode_id = ${episode.id})`;

    return {
      seriesId,
      nextSeason,
      nextEpisode,
      userId,
    };
  } catch (e) {
    console.log(e);
    if (e instanceof Error && e.name === "OverflowError") {
      console.error(`Error: ${e.message}, deleting entry ${seriesId}`);
      query`DELETE FROM nextup WHERE series_id = ${seriesId} AND user_id = ${userId}`;
    }
    throw createHttpError(404);
  }
}

export function remove(seriesId: string, userId: number) {
  const [[episodeId]] = query<
    [string]
  >`SELECT nextup.episode_id FROM nextup WHERE nextup.series_id = ${seriesId} AND nextup.user_id = ${userId}`;

  query`DELETE FROM nextup WHERE nextup.series_id = ${seriesId} AND nextup.user_id = ${userId}`;
  // Purge media rows that have no child references
  query`DELETE FROM media WHERE id = ${seriesId} AND NOT EXISTS (SELECT * FROM nextup WHERE series_id = ${seriesId})`;
  if (episodeId != null) {
    query`DELETE FROM media WHERE id = ${episodeId} AND NOT EXISTS (SELECT * FROM nextup WHERE episode_id = ${episodeId})`;
  }
}

function getNextEpisode(args: {
  currentEpisode: number;
  currentSeason: number;
  totalSeasons: number;
  episodesInSeason: number;
}) {
  let nextSeason: number = args.currentSeason;
  let nextEpisode: number = args.currentEpisode;
  const error = new Error("Season or episode overflow error!");
  error.name = "OverflowError";

  if (args.currentEpisode < args.episodesInSeason) {
    nextEpisode = args.currentEpisode + 1;
  }
  if (args.currentEpisode > args.episodesInSeason) {
    throw error;
  }
  if (args.currentEpisode === args.episodesInSeason) {
    if (args.currentSeason < args.totalSeasons) {
      nextSeason = args.currentSeason + 1;
      nextEpisode = 1;
    }
    if (args.currentSeason === args.totalSeasons) {
      throw error;
    }
    if (args.currentSeason > args.totalSeasons) {
      throw error;
    }
  }
  return {
    nextSeason,
    nextEpisode,
  };
}
