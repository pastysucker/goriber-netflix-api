import {
  embyScraper,
  fileSystemScraper,
  h5aiScraper,
  nginxScraper,
  sflixScraper,
  tmdbEngine,
} from "shared/inits.ts";
import { scrapingPipeline } from "shared/scraping-pipeline/scraping-pipeline.ts";
import { Episode, Season, Series } from "models/media/series.ts";

export function getOne(id: string) {
  return tmdbEngine.getSeries(id);
}

export function getSeasons(item: Series) {
  return tmdbEngine.getSeasons(item.id);
}

export function getSeason(item: Series, seasonIndex: number) {
  return tmdbEngine.getSeason(
    item.id,
    seasonIndex,
  );
}

export function getEpisodes(item: Series, seasonIndex: number) {
  return tmdbEngine.getEpisodes(
    item.id,
    seasonIndex,
  );
}

export function getEpisode(
  series: Series,
  seasonIndex: number,
  episodeIndex: number,
) {
  return tmdbEngine.getEpisode(
    series.id ?? "",
    seasonIndex,
    episodeIndex,
  );
}

export function getSources(series: Series, season: Season, episode: Episode) {
  return scrapingPipeline(
    [fileSystemScraper, embyScraper, h5aiScraper, nginxScraper, sflixScraper],
    {
      imdbId: episode.externalIds?.imdb,
      title: series.title,
      episodeIndex: episode.index,
      seasonIndex: season.index,
      isSeries: true,
      isNsfw: false,
    },
  );
}
