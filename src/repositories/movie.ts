import {
  embyScraper,
  h5aiScraper,
  perverzijaEngine,
  perverzijaScraper,
  sflixScraper,
  tmdbEngine,
} from "shared/inits.ts";
import { scrapingPipeline } from "shared/scraping-pipeline/scraping-pipeline.ts";
import { Movie } from "models/media/movie.ts";
import ItemType from "types/itemType.ts";

export function getOne(id: string, safeSearch = true) {
  return safeSearch ? tmdbEngine.getMovie(id) : perverzijaEngine.getMovie(id);
}

export function getSources(item: Movie, shortCircuit = true) {
  return scrapingPipeline(
    [embyScraper, h5aiScraper, sflixScraper, perverzijaScraper],
    {
      id: item.id,
      title: item.title,
      year: item.year,
      imdbId: item.externalIds?.imdb,
      isSeries: false,
      isNsfw: item.adult,
    },
    shortCircuit,
  );
}

export function getSimilar(id: string) {
  return tmdbEngine.getSimilarItems(
    id,
    ItemType.Movie,
  );
}
