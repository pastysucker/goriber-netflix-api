import { perverzijaEngine, tmdbEngine } from "shared/inits.ts";
import { parseTitleAndYear } from "video-filename-parser";

export function search(query: string) {
  const { title: q, year } = parseTitleAndYear(query);
  const safeSearch = !(q.startsWith("nsfw"));

  const title = safeSearch ? q : q.substring(5);
  return safeSearch
    ? tmdbEngine.multiSearch(
      title,
      !safeSearch,
      year != null ? parseInt(year) : undefined,
    )
    : perverzijaEngine.searchItems(title);
}
