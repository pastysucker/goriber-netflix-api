import { compileFileClient } from "pug";
import { walk } from "@std/fs/walk";

const VIEW_DIR = "src/server/views";
for await (const dir of walk(VIEW_DIR, { maxDepth: 1, exts: ["pug"] })) {
  const fixedName = dir.name.split(".")[0];
  const compiledFilename = "__" + fixedName + ".compiled.js";

  const compiledTemplate =
    'import * as pug from "pug/runtime";' +
    compileFileClient(dir.path, {
      name: fixedName,
      debug: false,
      filename: compiledFilename,
      inlineRuntimeFunctions: false,
    }) +
    "; export default " +
    fixedName;

  await Deno.writeTextFile(VIEW_DIR + "/" + compiledFilename, compiledTemplate);

  console.log("Compiled " + fixedName + " as " + compiledFilename);
}
