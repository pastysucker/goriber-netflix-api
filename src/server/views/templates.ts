import renderError from "server/views/__error.compiled.js";
import renderMovie from "server/views/__movie.compiled.js";
import renderSeries from "server/views/__series.compiled.js";
import renderEpisode from "server/views/__episode.compiled.js";
import renderSeason from "server/views/__season.compiled.js";
import renderIndex from "server/views/__index.compiled.js";
import renderTrakt from "server/views/__trakt.compiled.js";

export {
  renderEpisode,
  renderError,
  renderIndex,
  renderMovie,
  renderSeason,
  renderSeries,
  renderTrakt,
};
