import { walk } from "@std/fs/walk";
import { typeByExtension } from "@std/media-types";
import { extname } from "@std/path";
import { Base64 } from "bb64";

const OUTPUT_FILENAME = "compiledAssets.js";
const STATIC_DIR = "./src/server/views/static";

const file = await Deno.open(STATIC_DIR + "/" + OUTPUT_FILENAME, {
  append: true,
  create: true,
});
await file.lock();
await file.truncate();
const encoder = new TextEncoder();

await file.write(encoder.encode(`let files=new Map();`));

let index = 0;
for await (const dir of walk(STATIC_DIR, { maxDepth: 1 })) {
  if (
    !dir.isDirectory &&
    dir.name !== "__compiler.ts" &&
    !dir.name.includes(".compiled.js") &&
    !dir.name.includes(OUTPUT_FILENAME)
  ) {
    const encoded = Base64.fromFile(dir.path).toString();
    const mimeType = typeByExtension(extname(dir.name));

    const compiledAsset = `let file${index}="${encoded}";
let mimeType${index}="${mimeType}";
files.set("${dir.name}",{encoded:file${index},mimeType:mimeType${index}});
    `;

    await file.write(encoder.encode(compiledAsset));

    index += 1;
  }
}

await file.write(encoder.encode(`export default files;`));

console.log(`Encoded and bundled ${index} static assets in ${STATIC_DIR}`);
