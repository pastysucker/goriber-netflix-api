import log from "logger";
import { Application } from "oak/mod.ts";
import { oakCors } from "oakCors";
import { logger as loggerMiddleware } from "server/middleware/logger.ts";
import { responseTimeHeader } from "server/middleware/responseTime.ts";
import { serveAssets } from "server/middleware/serveAssets.ts";
import { apiRouter } from "server/routes/api/routes.ts";
import { viewRouter } from "server/routes/view/routes.ts";
import { readConfigFile } from "shared/config.ts";

const app = new Application<{ userId: number }>({
  state: {
    userId: 1,
  },
});

const abortController = new AbortController();

app.use(loggerMiddleware);
app.use(
  oakCors({
    methods: "GET",
    optionsSuccessStatus: 200,
  }),
);

app.use(responseTimeHeader);
app.use(apiRouter.routes(), apiRouter.allowedMethods());
app.use(viewRouter.routes(), viewRouter.allowedMethods());
app.use(serveAssets);

const port = Number(Deno.env.get("PORT") ?? "6767");

app.addEventListener("listen", (e) => {
  log.info(`Started listening on ${e.hostname}:${e.port}...`);
  readConfigFile().then((config) => {
    log.info(`Enabled scrapers: ${config.scrapers.enabled.join(", ")}`);
  });
});

app.addEventListener("error", (e) => {
  log.warn(e.message);
});

function signalHandler(signal: string) {
  return () => {
    console.log(`(${signal}) Exiting...`);
    abortController.abort();
    Deno.exit(0);
  };
}

Deno.addSignalListener("SIGINT", signalHandler("SIGINT"));
switch (Deno.build.os) {
  case "linux":
  case "darwin":
    Deno.addSignalListener("SIGTERM", signalHandler("SIGTERM"));
    break;
  case "windows":
    Deno.addSignalListener("SIGBREAK", signalHandler("SIGBREAK"));
}

await app.listen({ port, signal: abortController.signal });
