import { Middleware } from "oak/mod.ts";
import assets from "server/views/static/compiledAssets.js";

export const serveAssets: Middleware = (ctx) => {
  const requestedFile = ctx.request.url.pathname.substring(1);

  const decodedFile = () => {
    const encoded = assets.get(requestedFile)?.["encoded"];
    if (encoded != null) {
      return atob(encoded);
    }
  };

  const decoded = decodedFile();
  const mimeType = assets.get(requestedFile)?.["mimeType"];
  if (decoded != null && mimeType != null) {
    ctx.response.body = decoded;
    ctx.response.headers.set("content-type", mimeType);
    ctx.response.headers.set(
      "cache-control",
      "public, max-age=21600, stale-while-revalidate=129600",
    );
  } else {
    ctx.response.status == 404;
  }
};
