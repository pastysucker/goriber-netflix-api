import { Middleware } from "oak/mod.ts";
import { gray } from "@std/fmt/colors";
import log from "logger";

export const logger: Middleware = async (ctx, next) => {
  try {
    await next();
    log.info(
      `${ctx.request.method} ${ctx.response.status} - ${ctx.request.url.pathname}${ctx.request.url.search} - ${ctx.request.ip}`,
      +gray(ctx.request.headers.get("user-agent") ?? ""),
    );
  } catch (err) {
    throw err;
  }
};
