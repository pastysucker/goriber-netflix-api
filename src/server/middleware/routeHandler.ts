/// <reference types="types/types.d.ts" />

import type { RouterContext, RouterMiddleware } from "oak/mod.ts";
import { HttpError } from "oak/deps.ts";
import { ZodError } from "zod";
import { SqliteError } from "sqlite/src/error.ts";
import log from "logger";

type Body<T> = PromiseLike<T> | T;

export type Response<ResponseBody> = {
  status?: number;
  body?: Body<ResponseBody>;
};

type RouteHandler = <ResponseBody>(
  handler: (
    ctx: RouterContext<string>,
  ) => PromiseOr<Response<ResponseBody> | void>,
) => RouterMiddleware<string>;

const internalErrorMessage =
  "Internal server error. Contact administrator if error persists.";

export const routeHandler: RouteHandler =
  (handler) => async (ctx, _next): Promise<void> => {
    try {
      const result = await handler(ctx);
      if (result != null) {
        const { body, status } = result;
        if (body != null) {
          const resolvedBody = await body;
          ctx.response.body = { payload: resolvedBody };
        }
        ctx.response.status = status ?? 200;
      } else {
        ctx.response.status = 200;
        ctx.response.body = undefined;
      }
    } catch (error) {
      log.error(error);
      if (error instanceof HttpError) {
        ctx.response.body = {
          error: error.expose ? error.message : internalErrorMessage,
          status: error.status,
        };
        ctx.response.status = error.status;
      } else if (error instanceof ZodError) {
        ctx.response.body = {
          error: "Validation errors: " +
            error.errors.map((v) => v.path).join(", "),
          status: 400,
        };
        ctx.response.status = 400;
      } else if (error instanceof SqliteError) {
        if (error.code === 19) {
          // Constraint error
          ctx.response.body = {
            error: "Input is not acceptable, try again.",
            status: 400,
          };
          ctx.response.status = 400;
        } else {
          ctx.response.body = {
            error: "Encountered an unhandled database error.",
            status: 500,
          };
          ctx.response.status = 500;
        }
      } else if (error instanceof Error) {
        ctx.response.body = { error: internalErrorMessage, status: 500 };
        ctx.response.status = 500;
      } else {
        ctx.response.body = { error: internalErrorMessage, status: 500 };
        ctx.response.status = 500;
      }
    }
  };
