/// <reference types="../../types/types.d.ts" />

import type { RouterContext, RouterMiddleware } from "oak/mod.ts";
import { HttpError } from "oak/deps.ts";
import log from "logger";
import errorTemplate from "server/views/__error.compiled.js";

type RouteHandler = <T extends string>(
  handler: (ctx: RouterContext<string>) => PromiseOr<T>,
) => RouterMiddleware<string>;

export const viewHandler: RouteHandler =
  (handler) => async (ctx, _next): Promise<void> => {
    try {
      const result = await handler(ctx);
      ctx.response.body = result;
    } catch (error) {
      if (error instanceof HttpError) {
        log.error(error.message);
        ctx.response.body = errorTemplate({
          error: error.expose
            ? error.message
            : (error.status === 502
              ? error.message
              : "The server encountered an error. Contact your system administrator."),
          status: error.status,
        });
        ctx.response.status = error.status;
      } else {
        log.error(error.message);
        ctx.response.body = errorTemplate({
          error: error instanceof Error ? error.message : error.toString(),
          status: 500,
        });
        ctx.response.status = 500;
      }
    }
  };
