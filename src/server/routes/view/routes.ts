import { Router } from "oak/mod.ts";
import { mediaRouter } from "./media.ts";
import { userRouter } from "./user.ts";

export const viewRouter = new Router().use(
  mediaRouter.allowedMethods(),
  userRouter.allowedMethods(),
  mediaRouter.routes(),
  userRouter.routes(),
);
