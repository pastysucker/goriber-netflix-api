import { Router } from "oak/mod.ts";
import * as MovieRepository from "repositories/movie.ts";
import * as SeriesRepository from "repositories/series.ts";
import * as SearchRepository from "repositories/search.ts";
import { viewHandler } from "server/middleware/viewHandler.ts";
import {
  renderEpisode,
  renderIndex,
  renderMovie,
  renderSeason,
  renderSeries,
} from "server/views/templates.ts";
import log from "shared/logger.ts";
import { getQuery } from "shared/utils.ts";

const mediaRouter = new Router();

mediaRouter.use(async (ctx, next) => {
  await next();
  if (ctx.response.status >= 200 && ctx.response.status < 300) {
    ctx.response.headers.set(
      "cache-control",
      "public, max-age=21600, stale-while-revalidate=129600",
    );
  }
});

mediaRouter.get(
  "/movie/:id",
  viewHandler(async (ctx) => {
    const urlQuery = getQuery(ctx);
    const safeSearch = !urlQuery["include_adult"] as boolean;
    const shortCircuit = (() => {
      const shortCircuit = urlQuery["short_circuit"];
      return (shortCircuit == null || shortCircuit === "" ||
        shortCircuit === "true");
    })();

    const item = await MovieRepository.getOne(ctx.params.id ?? "", safeSearch);
    const sources = await MovieRepository.getSources(item, shortCircuit);
    return renderMovie({ item, sources });
  }),
);

mediaRouter.get(
  "/series/:id",
  viewHandler(async (ctx) => {
    const item = await SeriesRepository.getOne(ctx.params.id ?? "");
    const seasons = await SeriesRepository.getSeasons(item);

    return renderSeries({
      item,
      seasons,
    });
  }),
);

mediaRouter.get(
  "/series/:id/season/:seasonIndex",
  viewHandler(async (ctx) => {
    const { id, seasonIndex } = ctx.params;
    const series = await SeriesRepository.getOne(id ?? "");
    const season = await SeriesRepository.getSeason(
      series,
      Number(seasonIndex ?? "1"),
    );
    const episodes = await SeriesRepository.getEpisodes(
      series,
      Number(seasonIndex ?? "1"),
    );

    return renderSeason({
      series,
      season,
      episodes,
    });
  }),
);

mediaRouter.get(
  "/series/:id/season/:seasonIndex/episode/:episodeIndex",
  viewHandler(async (ctx) => {
    const { id, seasonIndex, episodeIndex } = ctx.params;
    const series = await SeriesRepository.getOne(id ?? "");
    const season = await SeriesRepository.getSeason(
      series,
      Number(seasonIndex ?? "1"),
    );
    const episode = await SeriesRepository.getEpisode(
      series,
      Number(seasonIndex),
      Number(episodeIndex),
    );
    const sources = await SeriesRepository.getSources(series, season, episode);
    return renderEpisode({
      series,
      season,
      episode,
      sources,
    });
  }),
);

mediaRouter.get(
  "/search",
  viewHandler(async (ctx) => {
    const urlQueries = getQuery(ctx, { asMap: true });
    const searchQuery = urlQueries.get("query") ?? "";

    const results = await SearchRepository.search(searchQuery);

    const template = renderIndex({
      results,
      query: searchQuery,
    });
    return template;
  }),
);

mediaRouter.get("/", (ctx) => {
  log.info("Serving homepage...");
  const template = renderIndex();
  ctx.response.body = template;
});

export { mediaRouter };
