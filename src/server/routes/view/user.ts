import {
  clientId,
  getToken,
  redirectUri,
  storeAccessToken,
  validateCredentials,
} from "shared/trakt/client.ts";
import { viewHandler } from "server/middleware/viewHandler.ts";
import { renderTrakt } from "server/views/templates.ts";
import { Router } from "oak/mod.ts";
import { getQuery } from "shared/utils.ts";

const userRouter = new Router();

userRouter.get(
  "/trakt",
  viewHandler(async (ctx) => {
    if (!validateCredentials()) {
      throw new Error("Trakt client parameters not defined!");
    }

    const { code } = getQuery(ctx);

    if (code != null) {
      const token = await getToken(code, "authorization_code");
      storeAccessToken(ctx.state.userId, token);
      return `Token is obtained`;
    } else {
      return renderTrakt({
        clientId,
        redirectUri: redirectUri,
      });
    }
  }),
);

export { userRouter };
