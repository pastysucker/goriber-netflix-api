import { Router } from "oak/mod.ts";
import { routeHandler } from "server/middleware/routeHandler.ts";
import { tmdbEngine } from "shared/inits.ts";
import ItemType from "types/itemType.ts";
import { getQuery } from "shared/utils.ts";

export const discoverRouter = new Router({ prefix: "/discover" });

const splitString = (string: string) => string.split(",");

discoverRouter.get(
  "/:itemType",
  routeHandler((ctx) => {
    const { networks, genres, people } = getQuery(ctx) as Record<
      string,
      string | undefined
    >;
    const { itemType } = ctx.params;

    const networks_ = networks != null ? splitString(networks) : undefined;
    const genres_ = genres != null ? splitString(genres) : undefined;
    const people_ = people != null ? splitString(people) : undefined;

    return {
      body: tmdbEngine.discoverItems(
        itemType === "movie"
          ? ItemType.Movie
          : itemType === "series"
          ? ItemType.Series
          : ctx.throw(400),
        {
          genres: genres_,
          networks: networks_,
          people: people_,
        },
      ),
    };
  }),
);
