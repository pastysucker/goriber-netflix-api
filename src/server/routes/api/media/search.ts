import { Router } from "oak/mod.ts";
import { routeHandler } from "server/middleware/routeHandler.ts";
import { tmdbEngine } from "shared/inits.ts";
import ItemType from "types/itemType.ts";
import * as SearchRepository from "repositories/search.ts";
import { getQuery } from "shared/utils.ts";

export const searchRouter = new Router({ prefix: "/search" });

searchRouter.get(
  "/multi",
  routeHandler((ctx) => {
    const { query } = getQuery(ctx);

    if (query != null) {
      return {
        body: SearchRepository.search(query),
      };
    } else return ctx.throw(400, "Bad query");
  }),
);

searchRouter.get(
  "/:itemType",
  routeHandler((ctx) => {
    const { query, itemType } = getQuery(ctx, { mergeParams: true });
    if (itemType === "movie" || itemType === "series") {
      if (query != null) {
        return {
          body: tmdbEngine.searchItems(
            query,
            itemType === "movie" ? ItemType.Movie : ItemType.Series,
          ),
        };
      } else return ctx.throw(400, "Bad query");
    } else {
      return ctx.throw(400, "Invalid itemType");
    }
  }),
);
