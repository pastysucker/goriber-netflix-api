import { Router } from "oak/mod.ts";
import { routeHandler } from "server/middleware/routeHandler.ts";
import { tmdbEngine } from "shared/inits.ts";
import { getQuery } from "shared/utils.ts";

export const personRouter = new Router({ prefix: "/person" });

personRouter.get(
  "/search",
  routeHandler((ctx) => {
    const { query } = getQuery(ctx);

    if (query != null) {
      return {
        body: tmdbEngine.searchPerson(query),
      };
    } else {
      return ctx.throw(400);
    }
  }),
);

personRouter.get(
  "/:personId",
  routeHandler((ctx) => {
    const { personId } = ctx.params;
    if (personId != null) {
      return { body: tmdbEngine.getPerson(personId) };
    } else {
      ctx.throw(400);
    }
  }),
);

personRouter.get(
  "/:personId/credits",
  routeHandler((ctx) => {
    const { personId } = ctx.params;
    if (personId != null) {
      return { body: tmdbEngine.getPersonCredits(personId) };
    } else {
      ctx.throw(400);
    }
  }),
);
