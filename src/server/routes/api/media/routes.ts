import { Router, RouterMiddleware } from "oak/mod.ts";
import { movieRouter } from "./movie.ts";
import { searchRouter } from "./search.ts";
import { seriesRouter } from "./series.ts";
import { discoverRouter } from "./discover.ts";
import { personRouter } from "./person.ts";

const cacheMiddleware: RouterMiddleware<string> = (async (ctx, next) => {
  await next();
  if (
    ctx.response.status >= 200 && ctx.response.status < 300 &&
    ctx.request.method === "GET"
  ) {
    if (!ctx.matched?.map((e) => e.path).includes("/api/users")) {
      ctx.response.headers.set(
        "cache-control",
        "public, max-age=21600, stale-while-revalidate=129600",
      );
    }
  }
});

export const mediaRouter = new Router().use(
  cacheMiddleware,
  movieRouter.routes(),
  movieRouter.allowedMethods(),
  seriesRouter.routes(),
  seriesRouter.allowedMethods(),
  searchRouter.routes(),
  searchRouter.allowedMethods(),
  discoverRouter.routes(),
  discoverRouter.allowedMethods(),
  personRouter.routes(),
  personRouter.allowedMethods(),
);
