import { Router } from "oak/mod.ts";
import { routeHandler } from "server/middleware/routeHandler.ts";
import * as MovieRepository from "repositories/movie.ts";
import { getQuery } from "shared/utils.ts";

export const movieRouter = new Router({ prefix: "/movie" });

movieRouter.get(
  "/:id/similar",
  routeHandler((ctx) => {
    if (ctx.params.id != null) {
      const id = ctx.params.id;
      return { body: MovieRepository.getSimilar(id) };
    } else return ctx.throw(400, "No id specified");
  }),
);

movieRouter.get(
  "/:id",
  routeHandler((ctx) => {
    const { id } = ctx.params;
    const query = getQuery(ctx);
    const safeSearch = query["include_adult"] !== "true";

    if (id != null) {
      return { body: MovieRepository.getOne(id, safeSearch) };
    } else return ctx.throw(400, "No id specified");
  }),
);

movieRouter.get(
  "/:id/sources",
  routeHandler(async (ctx) => {
    const { id } = ctx.params;
    const query = getQuery(ctx);
    const safeSearch = query["include_adult"] !== "true";

    if (id != null) {
      const item = await MovieRepository.getOne(id, safeSearch);
      const sources = MovieRepository.getSources(item);
      return { body: sources };
    } else return ctx.throw(400, "No id specified");
  }),
);
