import { Router } from "oak/mod.ts";
import { routeHandler } from "server/middleware/routeHandler.ts";
import {
  embyScraper,
  fileSystemScraper,
  h5aiScraper,
  nginxScraper,
  tmdbEngine,
} from "shared/inits.ts";
import ItemType from "types/itemType.ts";
import { scrapingPipeline } from "shared/scraping-pipeline/scraping-pipeline.ts";
import { getQuery } from "shared/utils.ts";

export const seriesRouter = new Router({ prefix: "/series" });

seriesRouter.get(
  "/search",
  routeHandler((ctx) => {
    const { query } = getQuery(ctx);
    return {
      body: tmdbEngine.searchItems(
        query,
        ItemType.Series,
      ),
    };
  }),
);

seriesRouter.get(
  "/:id/seasons",
  routeHandler((ctx) => {
    if (ctx.params.id != null) {
      const id = ctx.params.id;
      return { body: tmdbEngine.getSeasons(id) };
    } else return ctx.throw(400, "No id specified");
  }),
);

seriesRouter.get(
  "/:id/seasons/:seasonIndex",
  routeHandler((ctx) => {
    const { id, seasonIndex } = ctx.params;
    if (id != null && seasonIndex != null) {
      return { body: tmdbEngine.getSeason(id, Number(seasonIndex)) };
    } else return ctx.throw(400, "No id specified");
  }),
);

seriesRouter.get(
  "/:id/seasons/:seasonIndex/episodes/:episodeIndex",
  routeHandler((ctx) => {
    const { id, seasonIndex, episodeIndex } = ctx.params;
    if (id != null && seasonIndex != null && episodeIndex != null) {
      return {
        body: tmdbEngine.getEpisode(
          id,
          Number(seasonIndex),
          Number(episodeIndex),
        ),
      };
    } else return ctx.throw(400, "No id specified");
  }),
);

seriesRouter.get(
  "/:id/seasons/:seasonIndex/episodes",
  routeHandler((ctx) => {
    const { id, seasonIndex } = ctx.params;
    if (id != null && seasonIndex != null) {
      return { body: tmdbEngine.getEpisodes(id, Number(seasonIndex)) };
    } else return ctx.throw(400, "No id specified");
  }),
);

seriesRouter.get(
  "/:id/seasons/:seasonIndex/episodes/:episodeIndex/sources",
  routeHandler(async (ctx) => {
    const { id, seasonIndex, episodeIndex } = ctx.params;
    if (id != null && seasonIndex != null && episodeIndex != null) {
      const { title, adult } = await tmdbEngine.getSeries(id);
      const { externalIds } = await tmdbEngine.getEpisode(
        id,
        Number(seasonIndex),
        Number(episodeIndex),
      );
      const sources = scrapingPipeline([
        fileSystemScraper,
        embyScraper,
        h5aiScraper,
        nginxScraper,
      ], {
        imdbId: externalIds?.imdb,
        tvdbId: externalIds?.tvdb,
        title,
        episodeIndex: Number(episodeIndex),
        seasonIndex: Number(seasonIndex),
        isSeries: true,
        isNsfw: adult,
      });
      return { body: sources };
    } else return ctx.throw(400, "No id specified");
  }),
);

seriesRouter.get(
  "/:id/similar",
  routeHandler((ctx) => {
    const { id } = ctx.params;
    if (id != null) {
      return {
        body: tmdbEngine.getSimilarItems(
          id,
          ItemType.Series,
          // limit != null ? Number(limit) : undefined,
        ),
      };
    } else return ctx.throw(400, "No id specified");
  }),
);

seriesRouter.get(
  "/:id",
  routeHandler((ctx) => {
    const { id } = ctx.params;
    if (id != null) {
      return { body: tmdbEngine.getSeries(id) };
    } else return ctx.throw(400, "No id specified");
  }),
);
