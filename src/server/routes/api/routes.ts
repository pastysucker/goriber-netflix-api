import { Router } from "oak/mod.ts";
import { mediaRouter } from "./media/routes.ts";
import { userRouter } from "./user/routes.ts";

export const apiRouter = new Router().use(
  "/api",
  mediaRouter.routes(),
  mediaRouter.allowedMethods(),
  userRouter.routes(),
  userRouter.allowedMethods(),
);
