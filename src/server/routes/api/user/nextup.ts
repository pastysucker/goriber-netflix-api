import { z } from "zod";
import { routeHandler } from "server/middleware/routeHandler.ts";
import { Router } from "oak/mod.ts";
import * as NextUpRepository from "repositories/nextup.ts";

export const nextupRouter = new Router({ prefix: "/:userId/nextup" });

const nextupSchema = z.object({
  seriesId: z.string(),
  seasonIndex: z.string().transform((v) => parseInt(v)),
  episodeIndex: z.string().transform((v) => parseInt(v)),
});

nextupRouter.get(
  "/",
  routeHandler((ctx) => {
    const { userId } = ctx.params;
    if (userId == null) {
      return ctx.throw(400, "User ID not provided");
    }

    const entries = NextUpRepository.getAll(userId);

    return {
      body: entries,
    };
  }),
);

nextupRouter.get(
  "/:seriesId",
  routeHandler((ctx) => {
    const { seriesId, userId } = ctx.params;
    if (seriesId == null || userId == null) {
      return ctx.throw(400, "Bad series id");
    }

    const entry = NextUpRepository.getOne(seriesId, userId);

    return {
      body: entry,
    };
  }),
);

/**
 * update or create
 * @deprecated this is not maintained for now
 */
nextupRouter.put(
  "/:id",
  routeHandler((ctx) => {
    return ctx.throw(501);

    /*
    const { id, userId } = ctx.params;

    if (!ctx.request.hasBody) {
      return ctx.throw(400, "No body specified");
    }
    const body = await ctx.request.body({ type: "json" }).value;
    const { seasonIndex, episodeIndex } = nextupSchema.omit({ seriesId: true })
      .parse(
        body,
      );
    if (id != null) {
      db.query(
        "INSERT OR REPLACE INTO nextup (id, season_index, episode_index, user_id) VALUES (?, ?, ?, ?)",
        [
          id,
          seasonIndex,
          episodeIndex,
          userId,
        ],
      );
      return {
        status: 200,
      };
    } else {
      return ctx.throw(400, "Bad series id");
    }
    */
  }),
);

nextupRouter.delete(
  "/:seriesId",
  routeHandler((ctx) => {
    const { seriesId, userId } = ctx.params;
    if (seriesId == null || userId == null) {
      return ctx.throw(400, "Series id or user id is invalid");
    }
    NextUpRepository.remove(seriesId, parseInt(userId));
  }),
);

nextupRouter.post(
  "/create",
  routeHandler(async (ctx) => {
    const userId = z.string().transform(Number).parse(ctx.params.userId);

    const body = await ctx.request.body.json();
    const { seriesId, seasonIndex, episodeIndex } = nextupSchema.parse(body);

    const { nextSeason, nextEpisode } = await NextUpRepository.create(
      seriesId,
      seasonIndex,
      episodeIndex,
      userId,
    );

    return {
      status: 201,
      body: {
        seriesId,
        nextSeason,
        nextEpisode,
        userId,
      },
    };
  }),
);
