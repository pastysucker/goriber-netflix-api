import { Router } from "oak/mod.ts";
import { z } from "zod";
import { query } from "shared/db/sqlite.ts";
import { routeHandler } from "server/middleware/routeHandler.ts";

export const userDetailsRouter = new Router();

const userSchema = z.object({
  username: z.string(),
  admin: z.optional(z.boolean().transform(Number)),
});

type User = [id: number, username: string, admin: number];

userDetailsRouter.get(
  "/",
  routeHandler((_ctx) => {
    const values = query<User>`SELECT id, username, admin FROM users`;

    const asObject = values.map((v) => {
      const [id, username, admin] = v;
      return { id, username, admin: Boolean(admin) };
    });

    return {
      body: asObject,
    };
  }),
);

userDetailsRouter.get(
  "/:userId",
  routeHandler((ctx) => {
    const { userId } = ctx.params;
    const [
      first,
    ] = query<User>`SELECT id, username, admin FROM users WHERE id = ${userId}`;
    if (first == null) {
      return ctx.throw(404, `User id ${userId} not found`);
    }

    const [id, username, admin] = first;
    const asObject = { id, username, admin: Boolean(admin) };

    return {
      body: asObject,
    };
  }),
);

userDetailsRouter.post(
  "/create",
  routeHandler(async (ctx) => {
    const body = await ctx.request.body.json();
    const { username, admin } = userSchema.parse(body);

    query`INSERT INTO users (username, admin) values (${username}, ${
      admin ?? 0
    })`;

    const [
      first,
    ] = query<
      User
    >`SELECT id, username, admin FROM users WHERE username = ${username}`;

    if (first != null) {
      const [id, username, admin] = first;

      return {
        status: 201,
        body: {
          id,
          username,
          admin: Boolean(admin),
        },
      };
    } else {
      return ctx.throw(500, "User was not created. Please try again.");
    }
  }),
);

userDetailsRouter.delete(
  "/:userId",
  routeHandler((ctx) => {
    const { userId } = ctx.params;

    query`DELETE FROM users WHERE userId = ${userId}`;
  }),
);
