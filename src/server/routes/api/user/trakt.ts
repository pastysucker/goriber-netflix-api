import { routeHandler } from "server/middleware/routeHandler.ts";
import { Router, RouterMiddleware } from "oak/mod.ts";
import * as trakt from "shared/trakt/client.ts";
import { query } from "shared/db/sqlite.ts";
import ItemType from "types/itemType.ts";
import * as z from "zod";

export const traktRouter = new Router({ prefix: "/:userId/trakt" });

const idSchema = z.object({
  tmdb: z.number().optional(),
  tvdb: z.number().optional(),
  imdb: z.string().optional(),
});
const itemTypeSchema = z.union([
  z.literal("movie"),
  z.literal("series"),
  z.literal("episode"),
])
  .transform(
    (type) =>
      type === "movie"
        ? ItemType.Movie
        : type === "series"
        ? ItemType.Series
        : ItemType.Episode,
  );

const userExists: RouterMiddleware<string> = async (ctx, next) => {
  const { userId } = ctx.params;
  const [user] = query`SELECT id FROM users WHERE id = ${userId}`;

  if (user == null) {
    ctx.throw(401, "User not found in database");
  } else {
    await next();
  }
};

const traktActivated: RouterMiddleware<string> = async (ctx, next) => {
  const { userId } = ctx.params;

  const [user] =
    query`SELECT access_token FROM trakt WHERE user_id = ${userId}`;

  if (user != null) {
    await next();
  } else {
    ctx.throw(401, "Trakt not activated!");
  }
};

traktRouter.get(
  "/details",
  userExists,
  routeHandler((ctx) => {
    const { userId } = ctx.params;

    const [user] = query`SELECT access_token
      FROM trakt WHERE user_id = ${userId}`;

    return {
      body: {
        activated: user != null,
      },
    };
  }),
);

traktRouter.post(
  "/activate",
  userExists,
  routeHandler<void>(async (ctx) => {
    const { userId } = ctx.params;
    const body = await ctx.request.body.json();
    const token = trakt.tokenSchema.parse(body);

    trakt.storeAccessToken(parseInt(userId!), token);
  }),
);

traktRouter.get(
  "/deactivate",
  userExists,
  traktActivated,
  routeHandler<void>((ctx) => {
    const { userId } = ctx.params;
    trakt.deleteAccessToken(parseInt(userId!));
  }),
);

traktRouter.post(
  "/checkin/:itemType",
  userExists,
  traktActivated,
  routeHandler(async (ctx) => {
    const { userId, itemType } = ctx.params;
    const ids = idSchema.parse(await ctx.request.body.json());

    await trakt.checkIn(
      parseInt(userId!),
      ids,
      itemTypeSchema.parse(itemType),
    );
  }),
);

traktRouter.post(
  "/history/:itemType",
  userExists,
  traktActivated,
  routeHandler(async (ctx) => {
    const { userId, itemType } = ctx.params;
    const ids = idSchema.parse(await ctx.request.body.json());

    await trakt.addToHistory(
      parseInt(userId!),
      ids,
      itemTypeSchema.parse(itemType),
    );
  }),
);

traktRouter.get(
  "/watchlist",
  userExists,
  traktActivated,
  routeHandler((ctx) => {
    const { userId } = ctx.params;
    return {
      body: trakt.getWatchlist(parseInt(userId!), ItemType.Movie),
    };
  }),
);
