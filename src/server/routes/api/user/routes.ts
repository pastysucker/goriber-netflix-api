import { Router } from "oak/mod.ts";
import { userDetailsRouter } from "./user.ts";
import { favoritesRouter } from "./favorites.ts";
import { nextupRouter } from "./nextup.ts";
import { traktRouter } from "./trakt.ts";

export const userRouter = new Router().use(
  "/users",
  async (ctx, next) => {
    ctx.response.headers.set("cache-control", "no-store, max-age=0");
    await next();
  },
  userDetailsRouter.routes(),
  userDetailsRouter.allowedMethods(),
  favoritesRouter.routes(),
  favoritesRouter.allowedMethods(),
  nextupRouter.routes(),
  nextupRouter.allowedMethods(),
  traktRouter.routes(),
  traktRouter.allowedMethods(),
);
