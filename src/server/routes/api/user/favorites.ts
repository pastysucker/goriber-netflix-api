import { Router } from "oak/mod.ts";
import { query } from "shared/db/sqlite.ts";
import { SearchResult } from "models/media/common.ts";
import { tmdbEngine } from "shared/inits.ts";
import { routeHandler } from "server/middleware/routeHandler.ts";

export const favoritesRouter = new Router({ prefix: "/:userId/favorites" });

favoritesRouter.get(
  "/",
  routeHandler((ctx) => {
    const { userId } = ctx.params;

    const values = query<
      [
        string,
        string,
        number | undefined,
        string,
        string | undefined,
        string | undefined,
      ]
    >`
    SELECT
      media.id,
      media.name,
      media.year,
      media.media_type,
      media.i_primary,
      media.i_backdrop
    FROM favorites
    LEFT JOIN media
    ON media.id = favorites.media_id
    WHERE favorites.user_id = ${userId}
    `;

    const results = values.map<SearchResult>((v) => {
      const [id, name, year, mediaType, primary, backdrop] = v;

      return {
        id,
        isMovie: mediaType === "movie", // NOTE: May be changed in the future,
        name,
        year,
        adult: false,
        imageUris: {
          primary,
          backdrop,
        },
      };
    });
    return {
      body: results,
    };
  }),
);

favoritesRouter.get(
  "/:mediaId",
  routeHandler((ctx) => {
    const { userId, mediaId } = ctx.params;

    if (mediaId != null) {
      const [result] = query<
        [
          string,
          string,
          number | undefined,
          string,
          string | undefined,
          string | undefined,
        ]
      >`
      SELECT
        media.id,
        media.name,
        media.year,
        media.media_type,
        media.i_primary,
        media.i_backdrop
      FROM favorites
      LEFT JOIN media
      ON media.id = favorites.media_id
      WHERE media.id = ${mediaId}
      AND favorites.user_id = ${userId}`;

      if (result == null) {
        return ctx.throw(404, "Not favorited");
      }

      const [id, name, year, mediaType, primary, backdrop] = result;
      return {
        body: {
          id,
          isMovie: mediaType === "movie", // NOTE: May be changed in the future,
          name,
          year,
          imageUris: {
            primary,
            backdrop,
          },
        },
      };
    }
  }),
);

favoritesRouter.put(
  "/:mediaId",
  routeHandler(async (ctx) => {
    const { userId, mediaId } = ctx.params;

    if (mediaId != null) {
      const series = await tmdbEngine.getSeries(mediaId); // Might be changed
      query`
        INSERT OR IGNORE INTO media (id, name, media_type, year, i_primary, i_backdrop, i_logo, i_thumb, i_banner)
        VALUES (
          ${mediaId},
          ${series.title},
          'series',
          ${series.year},
          ${series.imageUris?.primary ?? null},
          ${series.imageUris?.backdrop ?? null},
          ${series.imageUris?.logo ?? null},
          ${series.imageUris?.thumb ?? null},
          ${series.imageUris?.banner ?? null}
        )
      `;
      query`INSERT INTO favorites (media_id, user_id) VALUES (${mediaId}, ${userId})`;
      return {
        status: 201,
      };
    } else {
      return ctx.throw(400, "Invalid id");
    }
  }),
);

favoritesRouter.delete(
  "/:mediaId",
  routeHandler((ctx) => {
    const { mediaId, userId } = ctx.params;
    if (mediaId != null) {
      query`DELETE FROM favorites WHERE media_id = ${mediaId} AND user_id = ${userId}`;
      query`DELETE FROM media WHERE id = ${mediaId} AND NOT EXISTS (SELECT * FROM favorites WHERE media_id = ${mediaId})`;
      return {
        status: 200,
      };
    } else {
      return ctx.throw(400, "Invalid id");
    }
  }),
);
