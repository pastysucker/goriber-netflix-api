import { Select } from "cliffy/prompt/mod.ts";
import { scrapingPipeline } from "shared/scraping-pipeline/scraping-pipeline.ts";
import {
  embyScraper,
  h5aiScraper,
  nginxScraper,
  tmdbEngine,
} from "shared/inits.ts";
import ItemType from "types/itemType.ts";
import { parseTitleAndYear } from "video-filename-parser";
import { openPlayer } from "cli/utils.ts";
import * as NextUpRepository from "repositories/nextup.ts";

const subMimes = ["application/x-subrip", "text/vtt", "text/vnd.dvb.subtitle"];

export async function movie(query: string, options: Record<string, unknown>) {
  const { title, year } = parseTitleAndYear(query);
  const items = await tmdbEngine.multiSearch(
    title,
    false,
    year != null ? parseInt(year) : undefined,
  );
  const id = await Select.prompt({
    message: "Select a movie",
    options: items.map((item) => ({
      name: `${item.name} (${item.year})`,
      value: item.id,
    })),
  });
  const movie = await tmdbEngine.getMovie(id);
  const sources = await scrapingPipeline(
    [embyScraper, h5aiScraper, nginxScraper],
    {
      title: movie.title ?? title,
      year: movie.year ?? (year != null ? parseInt(year) : undefined),
      isSeries: false,
      imdbId: movie.externalIds?.imdb,
      isNsfw: movie.adult,
    },
    options["shortCircuit"] as boolean,
  );

  const streamUri = await Select.prompt({
    message: "Select a source",
    options: sources
      .filter((item) => !subMimes.includes(item.mimeType!))
      .map((item) => ({
        name: [
          item.displayName,
          item.fileName,
          item.fileSize != null
            ? (item.fileSize / 1000000000).toFixed(3) + " GB"
            : "unknown filesize",
        ].join(", "),
        value: item.streamUri,
      })),
  });

  console.log(streamUri);

  if (options.play != false) {
    console.log();

    const process = openPlayer(options.player as string, {
      url: streamUri,
      title: `${movie.title} ${movie.year}`,
      subtitles: sources
        .filter((item) => subMimes.includes(item.mimeType!))
        .map((v) => v.streamUri),
      extraArgs: options["playerArgs"] as string[] | undefined,
    });

    process.ref();
  }
}

export async function series(query: string, options: Record<string, unknown>) {
  const items = await tmdbEngine.searchItems(query, ItemType.Series);
  const seriesId = await Select.prompt({
    message: "Select a series",
    options: items.map((item) => ({
      name: `${item.name} (${item.year})`,
      value: item.id,
    })),
  });
  const series = await tmdbEngine.getSeries(seriesId);
  const seasons = await tmdbEngine.getSeasons(seriesId);
  const seasonIndex = await Select.prompt({
    message: "Select season",
    options: seasons.map((item) => ({
      name: `${item.index}. ${item.name}`,
      value: String(item.index),
    })),
  });
  const episodes = await tmdbEngine.getEpisodes(seriesId, Number(seasonIndex));
  const episodeIndex = await Select.prompt({
    message: "Select episode",
    options: episodes.map((item) => ({
      name: `${item.index}. ${item.name}`,
      value: String(item.index),
    })),
  });
  const episode = await tmdbEngine.getEpisode(
    seriesId,
    Number(seasonIndex),
    Number(episodeIndex),
  );

  const sources = await scrapingPipeline(
    [embyScraper, h5aiScraper, nginxScraper],
    {
      isSeries: true,
      imdbId: episode.externalIds?.imdb,
      tvdbId: episode.externalIds?.tvdb,
      title: series.title,
      episodeIndex: episode.index,
      seasonIndex: Number(seasonIndex),
      isNsfw: false,
    },
    options["shortCircuit"] as boolean,
  );

  const streamUri = await Select.prompt({
    message: "Select a source",
    options: sources
      .filter((item) => !subMimes.includes(item.mimeType!))
      .map((item) => ({
        name: [
          item.displayName,
          item.fileName,
          item.fileSize != null
            ? (item.fileSize / 1000000000).toFixed(3) + " GB"
            : "unknown filesize",
        ].join(", "),
        value: item.streamUri,
      })),
  });

  console.log(streamUri);

  if (options.play != false) {
    const { nextSeason, nextEpisode } = await NextUpRepository.create(
      episode.seriesId,
      episode.seasonIndex,
      episode.index,
      options["profileId"] as number,
    );
    console.log(`Next up: ${nextSeason}x${nextEpisode}`);
    console.log();

    const process = openPlayer(options.player as string, {
      subtitles: sources
        .filter((item) => subMimes.includes(item.mimeType!))
        .map((v) => v.streamUri),
      extraArgs: options["playerArgs"] as string[] | undefined,
      title: `${series.title} Season ${seasonIndex} Episode ${episodeIndex}`,
      url: streamUri,
    });

    process.ref();
  }
}

export async function nextup(options: Record<string, unknown>) {
  const all = NextUpRepository.getAll(
    (options["profileId"] as number).toString(),
  ).reverse();

  let selected = {
    seriesName: "",
    seriesId: "",
    seasonIndex: 0,
    episodeIndex: 0,
  };

  if (options["first"]) {
    const { seriesName, seriesId, seasonIndex, episodeIndex } = all[0];
    selected = {
      seriesName: seriesName,
      seriesId: seriesId,
      seasonIndex: seasonIndex,
      episodeIndex: episodeIndex,
    };
  } else {
    const result = await Select.prompt({
      message: `Found ${all.length} results`,
      options: all.map((entry) => ({
        name:
          `${entry.seriesName} - ${entry.seasonIndex}x${entry.episodeIndex} - ${entry.episodeName}`,
        value: JSON.stringify({
          seriesName: entry.seriesName,
          seriesId: entry.seriesId,
          seasonIndex: entry.seasonIndex,
          episodeIndex: entry.episodeIndex,
        }),
      })),
    });
    selected = JSON.parse(result);
  }

  const episode = await tmdbEngine.getEpisode(
    selected.seriesId,
    selected.seasonIndex,
    selected.episodeIndex,
  );

  const sources = await scrapingPipeline(
    [embyScraper, h5aiScraper, nginxScraper],
    {
      isSeries: true,
      imdbId: episode.externalIds?.imdb,
      tvdbId: episode.externalIds?.tvdb,
      episodeIndex: episode.index,
      seasonIndex: episode.seasonIndex,
      title: selected.seriesName,
      isNsfw: false,
    },
    options["shortCircuit"] as boolean,
  );

  const streamUri = await Select.prompt({
    message:
      `Sources for ${selected.seriesName} ${selected.seasonIndex}x${selected.episodeIndex} - ${episode.name}`,
    options: sources
      .filter((item) => !subMimes.includes(item.mimeType!))
      .map((item) => ({
        name: [
          item.displayName,
          item.fileName,
          item.fileSize != null
            ? (item.fileSize / 1000000000).toFixed(3) + " GB"
            : "unknown filesize",
        ].join(", "),
        value: item.streamUri,
      })),
  });

  console.log(streamUri);

  if (options.play != false) {
    const { nextSeason, nextEpisode } = await NextUpRepository.create(
      episode.seriesId,
      episode.seasonIndex,
      episode.index,
      options["profileId"] as number,
    );
    console.log(`Next up: ${nextSeason}x${nextEpisode}`);
    console.log();

    const process = openPlayer(options.player as string, {
      url: streamUri,
      extraArgs: options["playerArgs"] as string[] | undefined,
      title:
        `${selected.seriesName} Season ${episode.seasonIndex} Episode ${episode.index}`,
      subtitles: sources
        .filter((item) => subMimes.includes(item.mimeType!))
        .map((v) => v.streamUri),
    });

    process.ref();
  }
}
