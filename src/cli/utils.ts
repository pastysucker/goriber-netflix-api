export type Args = {
  url: string;
  title?: string;
  language?: string;
  subtitles?: string[];
  extraArgs?: string[];
};

function generateArglistMPV(args: Args) {
  const list = [] as string[];
  if (args == null) {
    return [];
  }
  if (args.title != null) {
    list.push("--title=" + args.title);
  }
  if (args.subtitles != null && args.subtitles.length > 0) {
    for (const sub of args.subtitles) {
      list.push("--sub-file=" + sub);
    }
  }
  if (args.extraArgs != null) {
    list.push(...args.extraArgs);
  }
  list.push(args.url);
  return list;
}

function generateArglistVLC(args: Args) {
  const list = [] as string[];
  if (args == null) {
    return [];
  }
  if (args.subtitles != null && args.subtitles.length > 0) {
    list.push("--input-slave=" + args.subtitles.join("#"));
  }
  if (args.extraArgs != null) {
    list.push(...args.extraArgs);
  }
  list.push(args.url);
  return list;
}

export function openPlayer(player: string | boolean, args: Args) {
  function msg(playerName: string) {
    console.log(`Opening in ${playerName}...`);
  }
  switch (player) {
    case true:
    case "mpv":
      msg("MPV");
      return new Deno.Command("mpv", { args: generateArglistMPV(args) }).spawn();
    case "videolan":
    case "vlc":
      msg("VLC Media Player");
      return new Deno.Command("vlc", { args: generateArglistVLC(args) }).spawn();
    default:
      throw new Error("Unsupported player passed!");
  }
}
