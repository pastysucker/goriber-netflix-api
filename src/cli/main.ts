import { cac } from "cac";
import { Level } from "optic/mod.ts";
import log from "logger";
import { movie, nextup, series } from "cli/actions.ts";

const cli = cac("gornet");

cli.option(
  "-s, --short-circuit",
  "Return results after first scraper successfully completes",
  { default: true },
);
cli.option("-p, --player <name>", "Specify the player name", {
  default: "mpv",
});
cli.option("-a, --player-args <args>", "Additional args to pass to player");
cli.option("--no-play", "Don't open the player", { default: true });
cli.option("--verbose", "Output verbose messages on internal operations");

const VERSION = "1.0.0";

cli
  .command("movie <query>", "Search for a movie")
  .action(movie);

cli
  .command("series <query>", "Search for a series")
  .option("-u, --profile-id <id>", "Profile id", { default: 1 })
  .action(series);

cli
  .command("nextup", "List next up episodes")
  .option("-f, --first", "Automatically select first result")
  .option("-u, --profile-id <id>", "Profile id", { default: 1 })
  .action(nextup);

cli.help();

cli.version(VERSION);

cli.parse();

const isVerbose = cli.options?.["verbose"] === true;

if (isVerbose) {
  log.withMinLogLevel(Level.Debug);
} else {
  log.withMinLogLevel(Level.Info);
}
