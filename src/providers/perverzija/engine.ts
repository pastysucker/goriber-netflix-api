// deno-lint-ignore-file no-unused-vars
import { Engine } from "models/engine.ts";
import { Parser } from "models/parser.ts";
import { MediaSource, SearchResult } from "models/media/common.ts";
import ItemType from "types/itemType.ts";
import { fetchHTML, fetchJSON } from "providers/perverzija/client.ts";
import { noop } from "shared/utils.ts";
import { Movie } from "models/media/movie.ts";
import { Episode, Season, Series } from "models/media/series.ts";
import { DOMParser, initParser } from "deno_dom/deno-dom-wasm-noinit.ts";
import { parseMasterPlaylist } from "../../shared/m3u8-parser/mod.ts";

export class PerverzijaEngine implements Engine {
  readonly DOMAIN = "https://tube.perverzija.com";
  readonly parser: Parser;

  async searchItems(
    query: string,
  ): Promise<SearchResult[]> {
    await initParser();
    const parser = new DOMParser();

    const json = await fetchJSON(
      this.DOMAIN,
      "/search",
      new Map([["search", query]]),
    );
    const results = new Array<SearchResult>();

    for (const item of json) {
      results.push({
        id: item["id"].toString(),
        isMovie: true,
        name: parser.parseFromString(item["title"], "text/html")?.textContent!,
        adult: true,
        imageUris: {},
      });
    }

    return results;
  }

  async getPlaybackInfo(id: string): Promise<MediaSource[]> {
    const document = await fetchHTML(this.DOMAIN + "/?p=" + id);
    const embeddedVideoUrl = new URL(
      document.querySelector("#player div#player-embed iframe")?.getAttribute(
        "src",
      )!,
    );

    switch (embeddedVideoUrl.hostname) {
      case "pervl3.xtremestream.co":
      case "pervl2.xtremestream.co":
      case "pervl1.xtremestream.co":
      case "pervm1.xtremestream.co":
      case "perverzija.xtremestream.co":
      case "perv.xtremestream.co": {
        const videoId = embeddedVideoUrl.searchParams.get("data")!;
        const fullUrl = "https://" +
          embeddedVideoUrl.hostname +
          "/player/load_m3u8_xtremestream.php?data=" +
          videoId;
        const raw = await fetch(fullUrl).then((res) => res.text());
        const playlist = parseMasterPlaylist(raw);
        return playlist.sections.map((section) => ({
          streamUri: section.fullUri?.toString() ?? section.path,
          mimeType: "application/vnd.apple.mpegurl",
          fileName: videoId + "-" + (section.resolution ?? "xxx") + "@" +
            embeddedVideoUrl.hostname + ".m3u8",
          displayName: `${section.resolution ?? "NoRes"} (perverzija) | NSFW`,
        }));
      }
      default:
        return [];
    }
  }

  discoverItems(
    itemType: ItemType,
    args: {
      networks?: string[] | undefined;
      genres?: string[] | undefined;
      people?: string[] | undefined;
    },
  ): Promise<SearchResult[]> {
    return noop();
  }

  getSimilarItems(itemId: string, itemType: ItemType): Promise<SearchResult[]> {
    return noop();
  }

  getSeason(seriesId: string, seasonIndex: number): Promise<Season> {
    return noop();
  }

  async getMovie(id: string): Promise<Movie> {
    const json = await fetchJSON(this.DOMAIN, "/posts/" + id);

    await initParser();

    const parser = new DOMParser();
    const title = parser.parseFromString(json["title"]["rendered"], "text/html")
      ?.textContent;
    const synopsis =
      parser.parseFromString(json["excerpt"]["rendered"], "text/html")
        ?.getRootNode().textContent;
    const backdrop = json["yoast_head_json"]["og_image"][0]["url"];

    const movie = {
      id,
      title,
      adult: true,
      ageRating: "X",
      synopsis,
      imageUris: {
        backdrop: backdrop,
      },
    } as Movie;

    return movie;
  }
  getSeries(id: string): Promise<Series> {
    return noop();
  }
  getEpisode(
    seriesId: string,
    seasonIndex: number,
    episodeIndex: number,
  ): Promise<Episode> {
    return noop();
  }
  getSeasons(seriesId: string): Promise<Season[]> {
    return noop();
  }
  getEpisodes(seriesId: string, seasonIndex: number): Promise<Episode[]> {
    return noop();
  }
}
