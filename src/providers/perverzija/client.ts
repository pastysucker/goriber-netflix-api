/// <reference types="../../types/types.d.ts" />

import {
  DOMParser,
  HTMLDocument,
  initParser,
} from "deno_dom/deno-dom-wasm-noinit.ts";
import { createHttpError } from "oak/deps.ts";
import log from "shared/logger.ts";

export async function fetchHTML(
  url: string,
  queries?: Map<string, string>,
): Promise<HTMLDocument> {
  const controller = new AbortController();
  const timeout = setTimeout(() => {
    controller.abort();
  }, 15000); // will time out after 15000ms

  const finalUrl = url +
    (queries != null
      ? "?" + new URLSearchParams(queries as dynamic).toString()
      : "");

  await initParser();

  log.info("Making request to Perverzija");

  return fetch(finalUrl, { signal: controller.signal })
    .then((res) => {
      if (res.status === 404) {
        throw createHttpError(404);
      } else if (res.ok) {
        return res;
      } else {
        throw createHttpError(
          502,
          "perverzija is having problems. Try again later.",
        );
      }
    })
    .then((res) => res.text())
    .then((text) => new DOMParser().parseFromString(text, "text/html")!)
    .finally(() => {
      clearTimeout(timeout);
    });
}

export function fetchJSON(
  domain: string,
  endpoint: string,
  queries?: Map<string, string>,
): Promise<dynamic> {
  const controller = new AbortController();
  const timeout = setTimeout(() => {
    controller.abort();
  }, 15000); // will time out after 15000ms

  const finalUrl = domain + "/wp-json/wp/v2" + endpoint +
    (queries != null
      ? "?" + new URLSearchParams(queries as dynamic).toString()
      : "");

  log.info("Making request to Perverzija");

  return fetch(finalUrl, { signal: controller.signal })
    .then((res) => {
      if (res.status === 404) {
        throw createHttpError(404);
      } else if (res.ok) {
        return res;
      } else {
        throw createHttpError(
          502,
          "perverzija is having problems. Try again later.",
        );
      }
    })
    .then((res) => res.json())
    .finally(() => {
      clearTimeout(timeout);
    });
}
