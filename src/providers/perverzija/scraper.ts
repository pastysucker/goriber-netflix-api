import { createHttpError } from "oak/deps.ts";
import { Scraper, ScraperArgs } from "models/scraper.ts";
import { MediaSource } from "models/media/common.ts";
import { removeStopwords } from "shared/utils.ts";
import statcan from "shared/string-algorithms/statcan.ts";
import diceSimilarity from "shared/string-algorithms/dice-coefficient.ts";
import { PerverzijaEngine } from "providers/perverzija/engine.ts";

export class PerverzijaScraper implements Scraper {
  readonly engine: PerverzijaEngine;
  readonly name = "PerverzijaScraper";
  readonly nsfw = true;

  constructor(engine: PerverzijaEngine) {
    this.engine = engine;
  }

  async scrapeSources(args: ScraperArgs): Promise<MediaSource[]> {
    if (args.id != null) {
      return this.engine.getPlaybackInfo(args.id)
    }

    if (args.title == null) {
      throw createHttpError(404, "Media title unavailable!");
    }

    const title = args.title;

    const cleanTitle = removeStopwords(title);
    const soundexTitle = statcan(cleanTitle);

    const results = await this.engine.searchItems(args.title);
    const sortedSeries = results.sort((x, y) => {
      const distX = diceSimilarity(x.name ?? x.name, title);
      const distY = diceSimilarity(y.name ?? y.name, title);
      return distX === distY ? 0 : distX > distY ? -1 : 1;
    });
    const bestPicks = sortedSeries.slice(0, 5);

    const bestMatch = bestPicks.filter(
      (item) => statcan(removeStopwords(item.name)) === soundexTitle,
    )[0];

    return this.engine.getPlaybackInfo(bestMatch?.id ?? bestPicks[0].id);
  }
}
