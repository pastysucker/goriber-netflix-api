import { DirObject, NodeTreeBuilder } from "models/node-tree-builder.ts";
import {
  DOMParser,
  Element,
  initParser,
  NodeList,
  NodeType,
} from "deno_dom/deno-dom-wasm-noinit.ts";
import { filenameParse } from "video-filename-parser";
import { folderParse } from "providers/node-tree/folder-name-parser.ts";
import { fetchCached } from "shared/fetch_cached.ts";
import { isFile } from "shared/utils.ts";
import { typeByExtension } from "@std/media-types";
import { extname } from "@std/path";
import log from "logger";

export class NginxNodeBuilder implements NodeTreeBuilder {
  readonly name = "Nginx"

  async getParsedDoc(url: URL): Promise<NodeList> {
    log.info("Making request to " + url.hostname);
    log.debug("Full request url: " + url.toString());

    const res = await fetchCached(new Request(url.href), {
      addCustomCacheHeaders: true,
      maxAge: 10800,
    });
    if (!res.ok) {
      throw new Error("Failed request to " + url.host);
    }
    const html = await res.text();

    await initParser();
    const document = new DOMParser().parseFromString(html, "text/html");
    if (document == null) {
      throw new Error("Failed to parse HTML document");
    }
    return document.querySelectorAll("pre > a:not(:first-child)") as NodeList;
  }

  async getNodes(url: URL, isSeries: boolean): Promise<DirObject[]> {
    const parsedDoc = await this.getParsedDoc(url);
    const nodes: DirObject[] = [];
    for (const n of parsedDoc) {
      if (n.nodeType !== NodeType.ELEMENT_NODE) {
        continue;
      }
      const e = n as Element;

      const name = e.textContent.replace("/", "");
      const getIsFile = isFile(name);
      const href = e.getAttribute("href");

      if (href == null) {
        continue;
      }

      const fullHref = url + href;

      const node = {
        name,
        path: new URL(fullHref),
        isFile: getIsFile,
        mimeType: getIsFile ? typeByExtension(extname(name)) : undefined,
        meta: getIsFile ? filenameParse(name, isSeries) : folderParse(name),
      } as DirObject;
      nodes.push(node);
    }
    return nodes;
  }
}
