import { assert } from "std/testing/asserts.ts";
import { NginxNodeBuilder } from "../node-builder.ts";

Deno.test("should not throw exception when generating nginx node tree", async () => {
  const testURL =
    "https://cdn.naturalbd.com/tv-series/Zoey's.Extraordinary.Playlist.2020/Season%201/";

  const builder = new NginxNodeBuilder();
  const nodes = await builder.getNodes(
    new URL(testURL),
    true,
  );

  assert(nodes);
});
