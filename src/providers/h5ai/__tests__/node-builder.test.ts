import { assert } from "std/testing/asserts.ts";
import { H5aiNodeBuilder } from "../node-builder.ts";

Deno.test("should not throw exception when generating h5ai node tree", async () => {
  const testURL =
    "http://server4.ftpbd.net/FTP-4/English%20&%20Foreign%20TV%20Series/See%20(TV%20Series%202019%E2%80%93%20)%201080p/Season%202/";

  const builder = new H5aiNodeBuilder();
  assert(
    await builder.getNodes(
      new URL(testURL),
      true,
    ),
  );
});
