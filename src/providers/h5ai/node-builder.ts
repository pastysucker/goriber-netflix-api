import { DirObject, NodeTreeBuilder } from "models/node-tree-builder.ts";
import {
  DOMParser,
  Element,
  initParser,
  NodeList,
  NodeType,
} from "deno_dom/deno-dom-wasm-noinit.ts";
import { filenameParse } from "video-filename-parser";
import { folderParse } from "providers/node-tree/folder-name-parser.ts";
import { fetchCached } from "shared/fetch_cached.ts";
import { typeByExtension } from "@std/media-types";
import { extname } from "@std/path";
import { isFile, kbToBytes } from "shared/utils.ts";
import log from "logger";

export class H5aiNodeBuilder implements NodeTreeBuilder {
  readonly name = "H5ai";

  async getParsedDoc(url: URL): Promise<NodeList> {
    log.info("Making request to " + url.hostname);
    log.debug("Full request url: " + url.toString());

    const res = await fetchCached(new Request(url.href), {
      addCustomCacheHeaders: true,
      maxAge: 10800,
    });
    if (!res.ok) {
      throw new Error("Failed request to " + url.host);
    }
    const html = await res.text();

    await initParser();
    const document = new DOMParser().parseFromString(html, "text/html");
    if (document == null) {
      throw new Error("Document parsing failed");
    }
    return document.querySelectorAll(
      "table tr:not(:first-child):not(:nth-child(2))",
    ) as NodeList;
  }

  async getNodes(url: URL, isSeries: boolean): Promise<DirObject[]> {
    const parsedDoc = await this.getParsedDoc(url);
    const nodes: DirObject[] = [];
    for (const n of parsedDoc) {
      if (n.nodeType !== NodeType.ELEMENT_NODE) {
        continue;
      }
      const e = n as Element;
      const fbn = e.querySelector(".fb-n > a"); // name
      const fbs = e.querySelector(".fb-s"); // size
      const fbd = e.querySelector(".fb-d"); // date

      if (fbn == null || fbs == null || fbd == null) {
        continue;
      }

      const name = fbn.textContent;
      const getIsFile = isFile(name);
      const fileSize = kbToBytes(fbs.textContent);
      const date = new Date(fbd.textContent);
      const href = fbn.getAttribute("href");

      if (href == null) {
        continue;
      }

      const fullHref = url.origin + href;

      nodes.push({
        name,
        path: new URL(fullHref),
        isFile: getIsFile,
        lastModified: date,
        fileSize: getIsFile ? fileSize : undefined,
        mimeType: getIsFile ? typeByExtension(extname(name)) : undefined,
        meta: getIsFile ? filenameParse(name, isSeries) : folderParse(name),
      } as DirObject);
    }
    return nodes;
  }
}
