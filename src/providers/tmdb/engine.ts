/// <reference types="../../types/types.d.ts" />

import { Engine } from "models/engine.ts";
import { PersonResult, SearchResult } from "models/media/common.ts";
import { Person } from "models/media/person.ts";
import { Episode, Season, Series } from "models/media/series.ts";
import { Movie } from "models/media/movie.ts";
import { client } from "providers/tmdb/client.ts";
import { TMDbParser } from "providers/tmdb/parser.ts";
import ItemType from "types/itemType.ts";

function filterByVotes(votes: number) {
  return function (rawResults: dynamic[]) {
    return rawResults.filter((r) => r["vote_count"] > votes);
  };
}
function filterByAvailability() {
  return function (rawResults: dynamic[]) {
    return rawResults.filter((r) =>
      r != null
        ? new Date(r["first_air_date"] ?? r["release_date"]) < new Date()
        : true
    );
  };
}

export class TMDbEngine implements Engine {
  readonly parser: TMDbParser;

  constructor(tmdbParser: TMDbParser) {
    this.parser = tmdbParser;
  }

  searchItems(query: string, itemType: ItemType) {
    return client(
      `/search/${itemType}`,
      new Map([
        ["query", query],
        ["include_adult", "false"],
        ["language", "en-US"],
      ]),
    ).then((json) => {
      return (json["results"] as dynamic[]).map((v) =>
        this.parser.parseResult(v, itemType)
      );
    });
  }

  searchPerson(query: string): Promise<PersonResult[]> {
    return client(`/search/person`, new Map([["query", query]])).then(
      (json) => {
        return json["results"].map((v: dynamic) =>
          this.parser.parsePersonResult(v)
        );
      },
    );
  }

  multiSearch(query: string, nsfw = false, year?: number) {
    return client(
      `/search/multi`,
      new Map([
        ["query", query],
        ["primary_release_year", year != null ? year.toString() : ""],
        ["language", "en-US"],
        ["include_adult", String(nsfw)],
        ["region", "US"],
      ]),
    )
      .then(
        (json) =>
          (json["results"] as dynamic[]).filter(
            (v) => v["media_type"] !== "person",
          ),
      ).then((results) =>
        results.filter(
          (item) =>
            item["release_date"] != null
              ? (new Date(item["release_date"]) < new Date())
              : true,
        )
      )
      .then(
        (results) => results.map((v) => this.parser.parseResult(v)),
      );
  }

  async discoverItems(itemType: ItemType, args: {
    networks?: string[];
    genres?: string[];
    people?: string[];
  }): Promise<SearchResult[]> {
    const filtersApplied = (args.networks != null) ||
      (args.genres != null) ||
      (args.people != null);

    function request(page: number) {
      const params = new Map([
        ["include_adult", "false"],
        ["language", "en-US"],
        ["region", "US"],
        ["page", page.toString()],
      ]);

      if (args.networks != null) {
        params.set("with_networks", args.networks.join(","));
      }
      if (args.genres != null) {
        params.set("with_genres", args.genres.join(","));
      }
      if (args.people != null) {
        params.set("with_people", args.people.join(","));
      }
      return client(
        `/discover/${itemType}`,
        params,
      )
        .then((json) => json["results"] as dynamic[])
        .then(filterByVotes(filtersApplied ? 200 : 2500))
        .then(filterByAvailability());
    }
    const results = await Promise.all([1, 2].map((v) => request(v)));
    return results.flat().map((v) => this.parser.parseResult(v, itemType));
  }

  getSimilarItems(
    id: string,
    itemType: ItemType,
  ): Promise<SearchResult[]> {
    return client(
      `/${itemType}/${id}/similar`,
      new Map([
        ["language", "en-US"],
      ]),
    ).then((json) =>
      (json["results"] as dynamic[]).map((v) =>
        this.parser.parseResult(v, itemType)
      )
    );
  }

  getMovie(id: string): Promise<Movie> {
    return client(
      `/movie/${id}`,
      new Map([
        ["append_to_response", "release_dates,credits,external_ids,images"],
      ]),
    ).then((json) => this.parser.parseMovie(json));
  }

  getSeries(id: string): Promise<Series> {
    return client(
      `/tv/${id}`,
      new Map([
        ["append_to_response", "content_ratings,images,credits,external_ids"],
      ]),
    ).then((json) => this.parser.parseSeries(json));
  }

  getSeasons(seriesId: string): Promise<Season[]> {
    return client(`/tv/${seriesId}`)
      .then((json) => json["seasons"] as dynamic[])
      .then((seasons) =>
        seasons.filter(
          (season) =>
            season["air_date"] != null
              ? (new Date(season["air_date"]) < new Date())
              : true,
        )
      )
      .then((seasons) =>
        seasons.map((v) => this.parser.parseSeason(v, seriesId))
      ).then((seasons) => seasons.filter((v) => v.index !== 0));
  }

  getSeason(seriesId: string, seasonIndex: number): Promise<Season> {
    return client(`/tv/${seriesId}/season/${seasonIndex}`).then(
      (json) => this.parser.parseSeason(json, seriesId),
    );
  }

  getEpisodes(seriesId: string, seasonIndex: number): Promise<Episode[]> {
    return client(`/tv/${seriesId}/season/${seasonIndex}`)
      .then((json) =>
        (json["episodes"] as dynamic[]).filter(
          (episode) =>
            episode["air_date"] != null
              ? (new Date(episode["air_date"]) < new Date())
              : true,
        )
      )
      .then((episodes) =>
        episodes.map((v) => this.parser.parseEpisode(v, seriesId, seasonIndex))
      ).then((episodes) => episodes.filter((v) => v.index !== 0));
  }

  getEpisode(
    seriesId: string,
    seasonIndex: number,
    episodeIndex: number,
  ): Promise<Episode> {
    return client(
      `/tv/${seriesId}/season/${seasonIndex}/episode/${episodeIndex}`,
      new Map([
        ["append_to_response", "external_ids"],
      ]),
    ).then((json) => this.parser.parseEpisode(json, seriesId, seasonIndex));
  }

  getPerson(
    id: string,
  ): Promise<Person> {
    return client(`/person/${id}`).then(
      (json) => this.parser.parsePerson(json),
    );
  }

  getPersonCredits(id: string): Promise<SearchResult[]> {
    return client(`/person/${id}/combined_credits`).then(
      (json) => {
        const cast = (json["cast"] as dynamic[]).map<SearchResult>((
          v: dynamic,
        ) => this.parser.parseResult(v));
        const crew = (json["crew"] as dynamic[]).map<SearchResult>((
          v: dynamic,
        ) => this.parser.parseResult(v));
        return { cast, crew };
      },
    ).then(({ cast, crew }) => cast.concat(crew));
  }
}
