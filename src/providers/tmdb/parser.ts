import { Parser } from "models/parser.ts";
import { Movie } from "models/media/movie.ts";
import { Episode, Season, Series } from "models/media/series.ts";
import {
  Cast,
  ImageUris,
  Network,
  PersonResult,
  SearchResult,
} from "models/media/common.ts";
import { Person } from "models/media/person.ts";
import ItemType from "types/itemType.ts";

export class TMDbParser implements Parser {
  private readonly imagesBaseUrl = "https://image.tmdb.org/t/p" as const;

  parseAgeRating(json: dynamic): string | undefined {
    const usa: dynamic = (json["results"] as dynamic[]).find((e) =>
      e["iso_3166_1"] === "US"
    );
    const other: dynamic = (json["results"] as dynamic[])[0];

    const tvRating = (usa ?? other)?.["rating"];
    if (tvRating != null) return tvRating;

    const dates: dynamic[] | undefined = (usa ?? other)?.["release_dates"];

    const matchedDate = dates?.find((v: dynamic) => v["certification"] !== "");

    if (matchedDate != null) {
      return matchedDate["certification"];
    }
  }

  private parseCast(json: dynamic): Cast[] {
    return (json as dynamic[]).filter((v) =>
      v["known_for_department"] === "Acting"
    )
      .slice(0, 10)
      .map<
        Cast
      >((v) => ({
        id: v["id"],
        name: v["name"],
        role: v["character"],
        imageUris: this.buildImageUris(v),
      }));
  }

  private parseDirectors(json: dynamic): string[] {
    return (json as dynamic[]).filter((v) => v["job"] === "Director").map<
      string
    >(
      (v) => v["name"],
    );
  }

  private parseNetworks(json: dynamic[]): Network[] {
    return json.map((v) => ({
      id: v["id"],
      name: v["name"],
      imageUris: this.buildImageUris(v),
    }));
  }

  private parseGender(code: number) {
    switch (code) {
      case 0:
        return "unknown";
      case 1:
        return "female";
      case 2:
        return "male";
      case 3:
        return "non-binary";
      default:
        throw Error("Bad gender code");
    }
  }

  parseExtendedImageUris(json: dynamic): ImageUris {
    enum Strategy {
      TextFirst,
      PlainFirst,
    }
    const backdrops = json["backdrops"] as dynamic[];
    const logos = json["logos"] as dynamic[];
    const posters = json["posters"] as dynamic[];

    const findBestImage = (listOfImages: dynamic[], strategy: Strategy) => {
      switch (strategy) {
        case Strategy.PlainFirst:
          return listOfImages.find((v) => v["iso_639_1"] === null);
        case Strategy.TextFirst:
          return listOfImages.find((v) => v["iso_639_1"] === "en") ??
            listOfImages.find((v) => v["iso_639_1"] !== null);
        default:
          break;
      }
    };

    const buildUri = (
      size: string,
      strategy: Strategy,
      listOfImages: dynamic[] | undefined,
    ) =>
      listOfImages != null && listOfImages.length > 0
        ? this.imagesBaseUrl + "/" + size +
          (findBestImage(listOfImages, strategy) ?? listOfImages[0])[
            "file_path"
          ]
        : undefined;

    const imageUris: ImageUris = {
      backdrop: buildUri("w1280", Strategy.PlainFirst, backdrops),
      primary: buildUri("w500", Strategy.TextFirst, posters),
      logo: buildUri("w500", Strategy.TextFirst, logos),
    };
    return imageUris;
  }

  private buildImageUris(json: dynamic): ImageUris {
    const imageUris: ImageUris = {};

    if (
      Object.prototype.hasOwnProperty.call(json, "backdrop_path") &&
      json["backdrop_path"] != null
    ) {
      imageUris.backdrop = this.imagesBaseUrl + "/w1280" +
        json["backdrop_path"];
    }
    if (
      Object.prototype.hasOwnProperty.call(json, "still_path") &&
      json["still_path"] != null
    ) {
      imageUris.backdrop = this.imagesBaseUrl + "/w780" + json["still_path"];
    }
    if (
      Object.prototype.hasOwnProperty.call(json, "poster_path") &&
      json["poster_path"] != null
    ) {
      imageUris.primary = this.imagesBaseUrl + "/w500" + json["poster_path"];
    }
    if (
      Object.prototype.hasOwnProperty.call(json, "profile_path") &&
      json["profile_path"] != null
    ) {
      imageUris.primary = this.imagesBaseUrl + "/w300" + json["profile_path"];
    }
    if (
      Object.prototype.hasOwnProperty.call(json, "logo_path") &&
      json["logo_path"] != null
    ) {
      imageUris.logo = this.imagesBaseUrl + "/w500" + json["logo_path"];
    }
    return imageUris;
  }

  parseMovie(json: dynamic) {
    const movie = <Movie> {
      id: String(json["id"]),
      title: json["title"],
      year: new Date(json["release_date"]).getFullYear(),
      tagline: json["tagline"] != "" ? json["tagline"] : undefined,
      synopsis: json["overview"],
      externalIds: {
        imdb: json["external_ids"]["imdb_id"],
        tvdb: json["external_ids"]["tvdb_id"],
        tmdb: json["id"],
      },
      genres: json["genres"].map((i: dynamic) => i["name"] as string),
      studios: json["production_companies"].map((i: dynamic) =>
        i["name"] as string
      ),
      criticRatings: { tmdb: json["vote_average"] },
      runtime: json["runtime"],
      ageRating: json["release_dates"] != null
        ? this.parseAgeRating(json["release_dates"])
        : undefined,
      cast: json["credits"] != null
        ? this.parseCast(json["credits"]["cast"])
        : undefined,
      directors: json["credits"] != null
        ? this.parseDirectors(json["credits"]["crew"])
        : undefined,
      imageUris: this.parseExtendedImageUris(json["images"]),
      adult: json["adult"] ?? true,
    };

    return movie;
  }

  parseSeries(json: dynamic): Series {
    const series = <Series> {
      id: String(json["id"]),
      title: json["name"],
      year: json["first_air_date"] != null
        ? new Date(json["first_air_date"]).getFullYear()
        : undefined,
      synopsis: json["overview"],
      tagline: json["tagline"] === "" ? undefined : json["tagline"],
      ageRating: this.parseAgeRating(json["content_ratings"]),
      externalIds: {
        imdb: json["external_ids"]["imdb_id"],
        tvdb: json["external_ids"]["tvdb_id"],
        tmdb: json["id"],
      },
      genres: json["genres"].map((i: dynamic) => i["name"] as string),
      // studios: json["production_companies"].map((i: dynamic) =>
      //   i["name"] as string
      // ),
      criticRatings: {
        tmdb: json["vote_average"],
      },
      averageRuntime: json["episode_run_time"][0],
      // ageRating: this.parseAgeRating(dynamic["release_dates"]),
      cast: json["credits"] != null
        ? this.parseCast(json["credits"]["cast"])
        : undefined,
      hasEnded: json["status"] != null ? json["status"] === "Ended" : undefined,
      lastAired: json["last_air_date"],
      imageUris: this.parseExtendedImageUris(json["images"]),
      networks: this.parseNetworks(json["networks"]),
      childCount: json["number_of_seasons"] as number,
      adult: json["adult"] as boolean ?? false,
    };
    return series;
  }

  parseSeason(json: dynamic, seriesId: string) {
    const season = <Season> {
      name: json["name"],
      id: String(json["id"]),
      seriesId: seriesId,
      childCount: json["episode_count"] ?? json["episodes"]?.length,
      index: json["season_number"],
      imageUris: this.buildImageUris(json),
    };
    return season;
  }

  parseEpisode(json: dynamic, seriesId: string, seasonIndex: number) {
    const episode = <Episode> {
      id: String(json["id"]),
      name: json["name"],
      synopsis: (json["overview"] != null && json["overview"] !== "")
        ? json["overview"]
        : undefined,
      index: json["episode_number"],
      airDate: (json["air_date"] != null && json["air_date"] !== "")
        ? new Date(json["air_date"])
        : undefined,
      directors: this.parseDirectors(json["crew"]),
      seasonIndex,
      seriesId,
      imageUris: this.buildImageUris(json),
      externalIds: {
        imdb: json["external_ids"]?.["imdb_id"],
        tvdb: json["external_ids"]?.["tvdb_id"],
        tmdb: json["id"],
      },
    };

    return episode;
  }

  parsePerson(json: dynamic): Person {
    return {
      id: String(json["id"]),
      name: json["name"],
      gender: this.parseGender(json["gender"]),
      biography: json["biography"],
      externalIds: {
        imdb: json["imdb_id"],
        tmdb: json["id"],
      },
      birthplace: json["place_of_birth"],
      department: json["known_for_department"],
      imageUris: this.buildImageUris(json),
    };
  }

  parseResult = (json: dynamic, itemType?: ItemType) => {
    const releaseDate = (json["release_date"] || json["first_air_date"]) as
      | string
      | undefined;
    const result = <SearchResult> {
      id: String(json["id"]),
      name: json["title"] || json["name"],
      year: releaseDate != null
        ? new Date(releaseDate)
          .getFullYear()
        : undefined,
      isMovie: json["media_type"] != null
        ? json["media_type"] === "movie"
        : itemType === ItemType.Movie,
      imageUris: this.buildImageUris(json),
      adult: json["adult"] ?? true,
    };

    return result;
  };

  parsePersonResult(json: dynamic): PersonResult {
    return {
      id: String(json["id"]),
      name: json["name"],
      gender: this.parseGender(json["gender"]),
      department: json["known_for_department"],
      imageUris: this.buildImageUris(json),
    };
  }
}
