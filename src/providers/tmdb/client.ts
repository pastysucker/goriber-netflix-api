import { createHttpError } from "oak/deps.ts";
import { fetchCached } from "shared/fetch_cached.ts";
import { readEnv } from "shared/config.ts";

async function client(pathname: string, query?: ReadonlyMap<string, string>) {
  const baseUrl = "https://api.themoviedb.org/3" as const;
  const apiKey = await readEnv("TMDB_API_KEY");

  if (apiKey == null) {
    throw createHttpError(500, "TMDb API key is missing!");
  }

  const url = baseUrl +
    pathname +
    "?api_key=" +
    apiKey +
    (query != null ? "&" + new URLSearchParams(query as dynamic).toString() : "");

  const request = new Request(url, {
    headers: {
      Authorization: `Bearer ${apiKey}`,
      Accept: "application/json;charset=utf-8",
      "Content-Type": "application/json;charset=utf-8",
    },
  });

  try {
    const res = await fetchCached(request);
    if (res.ok) {
      return res.json();
    } else {
      console.log(await res.json());
      throw createHttpError(502, "TMDb response was not successful.");
    }
  } catch (e) {
    if (e instanceof Error) {
      console.error(e);
      throw createHttpError(502, "Something went wrong with TMDb.");
    }
  }
}

export { client };
