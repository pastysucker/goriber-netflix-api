import { createHttpError } from "oak/deps.ts";
import { Scraper, ScraperArgs } from "models/scraper.ts";
import {
  LanguageCode,
  MediaSource,
  SearchResult,
  SubtitleResult,
} from "models/media/common.ts";
import { Episode, Season } from "models/media/series.ts";
import { typeByExtension } from "@std/media-types";
import { extname } from "@std/path";
import { nodeIsElement, removeStopwords } from "shared/utils.ts";
import statcan from "shared/string-algorithms/statcan.ts";
import diceSimilarity from "shared/string-algorithms/dice-coefficient.ts";
import {
  DOMParser,
  Element,
  initParser,
} from "deno_dom/deno-dom-wasm-noinit.ts";
import { PuppeteerInstance } from "shared/puppeteer/scraper.ts";
import { parseMasterPlaylist } from "m3u8-parser";
import log from "logger";

const VALID_SERVERS = ["UpCloud", "Vidcloud", "Voe"] as const;

type Server = {
  id: string;
  name: typeof VALID_SERVERS[number];
};

function isValidServerName(val: string): val is Server["name"] {
  return VALID_SERVERS.map((a) => a.toLowerCase()).includes(val.toLowerCase());
}

export class SflixScraper extends Scraper {
  readonly name = "SflixScraper";
  readonly nsfw = false;
  readonly domain = "https://sflix.to" as const;

  constructor() {
    super();
    initParser();
  }

  async getSeasons(mediaId: string): Promise<Season[]> {
    const url = this.domain + "/ajax/v2/tv/seasons/" + mediaId;
    const text = await fetch(url).then((res) => res.text());
    const parser = new DOMParser();
    const document = parser.parseFromString(text, "text/html");

    const seasonsEl = document?.querySelectorAll(".ss-item");
    if (seasonsEl == null) {
      return [];
    }

    const seasons = new Array<Season>();
    for (const node of seasonsEl) {
      if (!nodeIsElement(node)) {
        continue;
      }
      const id = node.getAttribute("data-id");
      if (id == null) {
        continue;
      }
      const name = node.textContent.trim();
      const index = parseInt(name.substring(name.length - 2));
      seasons.push({
        id,
        name,
        seriesId: mediaId,
        index,
        childCount: -1,
      });
    }

    return seasons;
  }

  async getEpisodes(seasonId: string): Promise<Episode[]> {
    const url = this.domain + "/ajax/v2/season/episodes/" + seasonId;
    const text = await fetch(url).then((res) => res.text());

    const parser = new DOMParser();
    const document = parser.parseFromString(text, "text/html");

    const results = new Array<Episode>();

    const el = document?.querySelectorAll(".swiper-slide .episode-item");
    if (el == null) {
      return results;
    }

    for (const node of el) {
      if (!nodeIsElement(node)) {
        continue;
      }
      const id = node.getAttribute("data-id");
      const epNumberEl = node.querySelector(".episode-number")?.textContent
        .trim();
      if (id == null || epNumberEl == null) {
        continue;
      }
      const index = parseInt(
        epNumberEl.trim().substring(epNumberEl.length - 3),
      );
      const name = node.querySelector(".film-name")?.textContent.trim();
      results.push({
        id,
        index,
        seriesId: "?",
        name: name ?? "",
        seasonIndex: -1,
      });
    }

    return results;
  }

  async getServers(id: string, isMovie: boolean): Promise<Server[]> {
    const url = isMovie
      ? (this.domain + "/ajax/movie/episodes/" + id)
      : (this.domain + "/ajax/v2/episode/servers/" + id);
    const text = await fetch(url).then((res) => res.text());

    const parser = new DOMParser();
    const document = parser.parseFromString(text, "text/html");

    const results = new Array<Server>();

    const items = document?.querySelectorAll("ul > li > a");
    if (items == null) {
      return results;
    }

    for (const i of items) {
      const item = i as Element;
      const id = item.getAttribute("data-id");
      if (id == null) continue;

      const name = item.getElementsByTagName("span")[0].textContent;
      if (isValidServerName(name)) {
        results.push({
          name: name,
          id: id,
        });
      }
    }

    return results;
  }

  async searchItems(arg: string, isSeries: boolean) {
    const text = await fetch(this.domain + `/ajax/search`, {
      method: "POST",
      body: new URLSearchParams({ keyword: arg }),
      headers: {
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "x-requested-with": "XMLHttpRequest",
        "user-agent":
          "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36",
        "referer": this.domain,
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
        "cookie": "show_share=true",
      },
    }).then((res) => res.text());

    const parser = new DOMParser();
    const document = parser.parseFromString(text, "text/html");

    const results = [] as SearchResult[];

    const items = document?.querySelectorAll(".nav-item:not(:last-of-type)");
    if (items == null) {
      return results;
    }

    for (const i of items) {
      const item = i as Element;
      const name = item.querySelector("h3.film-name")?.innerText;
      const isMovie =
        item.querySelector(".film-infor > span:last-of-type")?.innerText ===
          "Movie";
      const year = isMovie
        ? parseInt(
          item.querySelector(".film-infor > span:first-of-type")?.innerText ??
            "NaN",
        )
        : undefined;
      const poster = item
        .querySelector(".film-poster > img")
        ?.getAttribute("src");
      const href = item.getAttribute("href");
      if (name == null || isMovie == null || href == null) {
        continue;
      }

      if (isSeries === isMovie) {
        continue;
      }

      results.push({
        id: href,
        name,
        isMovie,
        year,
        adult: false,
        imageUris: {
          primary: poster ?? undefined,
        },
      });
    }

    return results;
  }

  async scrapeSources(args: ScraperArgs): Promise<MediaSource[]> {
    if (args.title == null) {
      throw createHttpError(404, "Media title unavailable!");
    }

    const title = args.title;
    const cleanTitle = removeStopwords(args.title);
    const soundexTitle = statcan(cleanTitle);

    log.info(
      `Started searching for ${
        args.isSeries ? "series" : "movies"
      } with title ${args.title}`,
    );
    const items = await this.searchItems(title, args.isSeries);

    const sortedSeries = items.sort((x, y) => {
      const distX = diceSimilarity(x.name ?? x.name, title);
      const distY = diceSimilarity(y.name ?? y.name, title);
      return distX === distY ? 0 : distX > distY ? -1 : 1;
    });
    const bestPicks = sortedSeries.slice(0, 5);

    const bestMatch = bestPicks.filter(
      (item) => statcan(removeStopwords(item.name)) === soundexTitle,
    )[0];

    const href = bestMatch?.id;
    if (href == null) {
      throw createHttpError(404, "No sources found");
    }
    const mediaSlug = href.split("/").at(-1);
    const mediaId = href.split("-").at(-1)!;

    log.debug(`Found item with slug ${mediaSlug}`);

    const servers = await (async () => {
      if (
        args.isSeries && args.seasonIndex != null && args.episodeIndex != null
      ) {
        log.debug(`Trying to fetch seasons...`);
        const seasons = await this.getSeasons(mediaId);
        const season = seasons.find((v) => v.index === args.seasonIndex);
        if (season != null) {
          log.debug(`Found season with index ${season.index}`);
          const episode = (await this.getEpisodes(season.id)).find((v) =>
            v.index === args.episodeIndex
          );
          if (episode != null) {
            log.debug(`Found episode with index ${episode.index}`);
            return this.getServers(episode.id, false);
          }
        }
      }
      return this.getServers(mediaId, true);
    })();

    const browser = new PuppeteerInstance();
    await browser.init();
    log.debug(
      `Puppeteer: Using ${browser.strategy.type} strategy`,
      browser.strategy,
    );

    const sources = new Array<MediaSource>();

    let i = 0, x = 0;
    for (const server of servers) {
      const watchPageUrl = this.domain +
        `/watch-${args.isSeries ? "tv" : "movie"}/${mediaSlug}.${server.id}`;

      if (args.isSeries) {
        log.debug(
          `Trying to fetch playback info for ${href}, season ${args.seasonIndex}, episode ${args.episodeIndex}`,
        );
      } else {
        log.debug(`Trying to fetch playback info for ${href}`);
      }

      const result = await browser.newPage(async (page) => {
        const sourcesRes = page.waitForResponse(
          (res) => res.ok() && res.url().includes("getSources"),
          { timeout: 30 * 1000 },
        )
          .catch((e) => {
            log.debug(e.message);
            return null;
          });
        const playlistResA = page.waitForResponse(
          (res) =>
            res.ok() &&
            res.headers()["content-type"]?.toLowerCase()?.includes("mpegurl"),
          { timeout: 30 * 1000 },
        )
          .catch((e) => {
            log.debug(e.message);
            return null;
          });
        await page.goto(watchPageUrl, {
          waitUntil: "networkidle0",
          timeout: 45 * 1000,
        });
        const subtitles = await sourcesRes.then((res) => res?.json()).then(
          (json) => {
            if (json == null) return [];

            return (json["tracks"] as dynamic[])
              .filter((i) => i["kind"] === "captions")
              .map<SubtitleResult>((item) => ({
                url: item["file"],
                language: item["label"],
              }));
          },
        );
        const playlistRes = await playlistResA;
        if (playlistRes == null) {
          return null;
        }

        const playlist = await playlistRes
          .text()
          .then((text) => parseMasterPlaylist(text, playlistRes.url()));

        return {
          playlist,
          subtitles,
        };
      });

      if (result == null) {
        log.error(`Error while fetching sources from ${server.name}`);
        continue;
      }

      if (result.playlist.sections.length == 0) {
        continue;
      }
      for (const section of result.playlist.sections) {
        i += 1;
        sources.push({
          displayName:
            this.domain.replaceAll("https://", "").replaceAll("http://", "") +
            ` ${section.resolution} #${i}`,
          fileName: mediaSlug + "@" + new URL(watchPageUrl).hostname + ".m3u8",
          streamUri: section.fullUri!.href,
          mimeType: typeByExtension(extname(section.path)),
        });
      }
      for (const sub of result.subtitles) {
        const langKey = LanguageCode.searchString(sub.language);
        if (langKey != null) {
          x += 1;
          sources.push({
            displayName: sub.language + " Subtitle",
            language: langKey,
            fileName: "subtitle_" + x + extname(sub.url),
            streamUri: sub.url,
            mimeType: typeByExtension(extname(sub.url)),
          });
        }
      }
      break;
    }

    await browser.close();

    return sources;
  }
}
