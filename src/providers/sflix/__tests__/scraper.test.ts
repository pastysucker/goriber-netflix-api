import { assert } from "std/testing/asserts.ts";
import { SflixScraper } from "../scraper.ts";

const scraper = new SflixScraper();

Deno.test("check searching series works", async () => {
  const items = await scraper.searchItems("The Boys", true);
  assert(items.length > 0);
});

Deno.test("check searching movies works", async () => {
  const items = await scraper.searchItems("Day after tomorrow", false);
  assert(items.length > 0);
});

Deno.test("correctly fetches movie servers", async () => {
  const result = await scraper.getServers("18921", true);
  assert(result.length > 0);
});

Deno.test("correctly fetches episode servers", async () => {
  const result = await scraper.getServers("1277831", false);
  assert(result.length > 0);
});

Deno.test("fetching seasons", async ()=>{
  const result = await scraper.getSeasons("84479")
  assert(result.length === 1)
})

Deno.test("fetching episodes", async ()=>{
  const result = await scraper.getEpisodes("70079")
  console.log(result)
})

Deno.test("correctly scrapes movie sources", async () => {
  const sources = await scraper.scrapeSources({
    isSeries: false,
    isNsfw: false,
    title: "Interstellar",
  });
  assert(sources.length === 0)
});


