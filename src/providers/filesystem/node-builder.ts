import { DirObject, NodeTreeBuilder } from "models/node-tree-builder.ts";
import { filenameParse } from "video-filename-parser";
import { folderParse } from "providers/node-tree/folder-name-parser.ts";

export class FileSystemNodeBuilder implements NodeTreeBuilder {
  readonly name = "Filesystem";

  async getNodes(dir: URL, isSeries: boolean): Promise<DirObject[]> {
    const nodes: DirObject[] = [];
    for await (const node of Deno.readDir(dir)) {
      const fullPath = decodeURIComponent(dir.pathname) + "/" + node.name;

      const stat = await Deno.stat(fullPath);

      if (stat.isSymlink) {
        // Ignore symlinks
        continue;
      }

      nodes.push({
        name: node.name,
        isFile: node.isFile,
        lastModified: stat.mtime ?? undefined,
        path: new URL("file://" + encodeURI(fullPath)),
        fileSize: stat.size,
        meta: stat.isFile
          ? filenameParse(node.name, isSeries)
          : folderParse(node.name),
      });
    }
    return nodes;
  }
}
