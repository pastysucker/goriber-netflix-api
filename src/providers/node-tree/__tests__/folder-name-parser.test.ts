import { assertEquals } from "std/testing/asserts.ts";
import { folderParse } from "../folder-name-parser.ts";
import { Resolution } from "video-filename-parser";
import { ParsedFolder } from "models/node-tree-builder.ts";

Deno.test("should parse parent folder titles", () => {
  const cases: [string, ParsedFolder][] = [
    ["Batwoman (TV Series 2019-) 720p", {
      title: "Batwoman",
      year: "2019",
      seasons: undefined,
      dubbing: undefined,
      resolution: Resolution.R720P,
      sources: [],
      edition: {},
      isFolder: true,
    }],
    ["Chesapeake Shores (2016)", {
      title: "Chesapeake Shores",
      year: "2016",
      seasons: undefined,
      dubbing: undefined,
      resolution: undefined,
      sources: [],
      edition: {},
      isFolder: true,
    }],
    ["Two Broke Girls - 2011 - 720p", {
      title: "Two Broke Girls",
      year: "2011",
      seasons: undefined,
      dubbing: undefined,
      resolution: Resolution.R720P,
      sources: [],
      edition: {},
      isFolder: true,
    }],
    ["The Insurgent 2015 720p", {
      title: "The Insurgent",
      year: "2015",
      seasons: undefined,
      dubbing: undefined,
      resolution: Resolution.R720P,
      sources: [],
      edition: {},
      isFolder: true,
    }],
    ["The.Marvelous.Mrs..Maisel.2017", {
      title: "The.Marvelous.Mrs..Maisel",
      year: "2017",
      seasons: undefined,
      dubbing: undefined,
      resolution: undefined,
      sources: [],
      edition: {},
      isFolder: true,
    }],
    ["Back to the Future (1985) 720p", {
      title: "Back to the Future",
      year: "1985",
      seasons: undefined,
      dubbing: undefined,
      resolution: Resolution.R720P,
      sources: [],
      edition: {},
      isFolder: true,
    }],
    ["Back to the Future II (1989) 720p", {
      title: "Back to the Future II",
      year: "1989",
      seasons: undefined,
      dubbing: undefined,
      resolution: Resolution.R720P,
      sources: [],
      edition: {},
      isFolder: true,
    }],
    ["Batman.Begins.2005", {
      title: "Batman.Begins",
      year: "2005",
      seasons: undefined,
      dubbing: undefined,
      resolution: undefined,
      sources: [],
      edition: {},
      isFolder: true,
    }],
  ];

  for (const [name, expectedParsing] of cases) {
    const testParsed = folderParse(name);
    assertEquals(testParsed, expectedParsing);
  }
});

Deno.test("should parse folders with season names", () => {
  const cases: [string, ParsedFolder][] = [
    ["Season 1", {
      title: undefined,
      year: undefined,
      edition: {},
      sources: [],
      seasons: [1],
      dubbing: undefined,
      resolution: undefined,
      isFolder: true,
    }],
  ];

  for (const [name, expectedParsing] of cases) {
    const testParsed = folderParse(name);
    assertEquals(testParsed, expectedParsing);
  }
});
