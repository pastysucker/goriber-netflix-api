import { Scraper, ScraperArgs } from "models/scraper.ts";
import { MediaSource } from "models/media/common.ts";
import { createHttpError } from "oak/deps.ts";
import { ParsedFilename, ParsedMovie, ParsedShow } from "video-filename-parser";
import {
  DirObject,
  NodeTreeBuilder,
  ParsedFolder,
} from "models/node-tree-builder.ts";
import { isYearInRange, removeStopwords } from "shared/utils.ts";
import statcan from "shared/string-algorithms/statcan.ts";
import diceSimilarity from "shared/string-algorithms/dice-coefficient.ts";
import log from "logger";

function isSeriesFile(meta: unknown): meta is ParsedShow {
  return Object.prototype.hasOwnProperty.call(meta, "isTv") === true;
}

function isMovieFile(meta: unknown): meta is ParsedMovie {
  return Object.prototype.hasOwnProperty.call(meta, "isTv") === false;
}

function isFolder(meta: unknown): meta is ParsedFolder {
  return Object.prototype.hasOwnProperty.call(meta, "isFolder") === true;
}

export class NodeTreeScraper implements Scraper {
  readonly name = "NodeTreeScraper";
  readonly nodeTree: NodeTreeBuilder;
  readonly nodeTreePaths: {
    movie: URL[];
    series: URL[];
  };
  readonly urlTransformer?: (url: URL) => URL;
  readonly nsfw = false;

  constructor(
    nodeTree: NodeTreeBuilder,
    nodeTreePaths: {
      movie: URL[];
      series: URL[];
    },
    opts?: {
      urlTransformer?: (url: URL) => URL;
    },
  ) {
    this.nodeTree = nodeTree;
    this.nodeTreePaths = nodeTreePaths;
    this.urlTransformer = opts?.urlTransformer;
  }

  async filterDirs(
    path: URL,
    title: string,
  ): Promise<DirObject[]> {
    const cleanTitle = removeStopwords(title);
    const soundexTitle = statcan(cleanTitle);

    const directory = await this.nodeTree.getNodes(path, false);

    const sortedSeries = directory.sort((x, y) => {
      const distX = diceSimilarity(x.meta?.title ?? x.name, title);
      const distY = diceSimilarity(y.meta?.title ?? y.name, title);
      return distX === distY ? 0 : distX > distY ? -1 : 1;
    });
    const bestPicks = sortedSeries
      .slice(0, 10);

    const similarSounds = bestPicks.filter((dir) =>
      statcan(removeStopwords(dir.meta?.title ?? dir.name)) ===
        soundexTitle
    );

    return similarSounds;
  }

  async scrapeMovies(args: ScraperArgs): Promise<MediaSource[]> {
    const { title, year } = args;

    if (title != null && year != null) {
      let movies: DirObject[] = [];

      const findMovies = async (
        nodes: DirObject[],
      ): Promise<DirObject[]> => {
        let movies: DirObject[] = [];
        for (const node of nodes) {
          if (isFolder(node.meta)) {
            const nodes = await this.nodeTree.getNodes(node.path, false);
            const nestedNodes = await findMovies(nodes);
            movies = movies.concat(nestedNodes);
          } else if (isMovieFile(node.meta)) {
            if (node.meta.year != null && node.meta.year != String(year)) {
              log.debug("File year not matched, ignoring " + node.name);
              continue;
            }
            if (node.name == "playlist.m3u8") {
              log.debug("Ignoring dummy file named " + node.name);
              continue;
            }
            if (
              node.mimeType != null && !node.mimeType.includes("mp2t") &&
              (node.mimeType.includes("video") ||
                node.mimeType.includes("mpegurl"))
            ) {
              log.debug("Matched a valid file: " + node.name);
              movies.push(node);
            }
          }
        }
        return movies;
      };

      for (const path of this.nodeTreePaths.movie) {
        const nodes = await this.nodeTree.getNodes(path, false);
        const node = nodes.find((node) => {
          const regex1 = /\(?(?<year>\d{4})\)?.+(&|and).+before/i; // (1995) & Before
          const regex2 = /(?<start>\d{1,4})-(?<end>\d{1,4})/i; // 0-1945

          if (node.name == String(year)) {
            return node;
          } else if (
            regex1.test(node.name)
          ) {
            const matchedYear = regex1.exec(node.name)?.groups?.["year"];
            if (matchedYear != null && year < parseInt(matchedYear)) {
              return node;
            }
          } else if (regex2.test(node.name)) {
            const groups = regex2.exec(node.name)?.groups;
            if (groups?.["start"] != null && groups?.["end"]) {
              const yearInRange = isYearInRange(
                year,
                Number(groups?.["start"]),
                Number(groups?.["end"]),
              );
              if (yearInRange) {
                return node;
              }
            }
          }
        });
        if (node != null) {
          const nodes = await this.filterDirs(node.path, title);
          const deepMovies = await findMovies(nodes);
          movies = movies.concat(deepMovies);
        }
      }

      const sources = movies.map<MediaSource | null>(
        (movie, index) => {
          const mimeType = movie.mimeType;

          if (
            mimeType != null &&
            !(mimeType.includes("video") || mimeType.includes("mpegurl"))
          ) {
            return null;
          }

          const { resolution, videoCodec, sources } = movie
            .meta as ParsedFilename;
          const displayName = [
            resolution,
            videoCodec,
            sources[0],
          ]
            .filter((v) => v != null)
            .join(" ").trim();

          return {
            streamUri: this.urlTransformer != null
              ? this.urlTransformer(movie.path).toString()
              : movie.path.toString(),
            fileName: movie.name,
            fileSize: movie.fileSize ?? undefined,
            mimeType: mimeType,
            displayName: (displayName != null && displayName !== "")
              ? displayName
              : movie.lastModified != null
              ? (`Added on ${movie.lastModified.toLocaleDateString()}`)
              : `Source #${index + 1}`,
          };
        },
      ).filter((v) => v != null) as MediaSource[];

      return sources;
    } else {
      throw createHttpError(400, `Invalid args passed to ${this.name}`);
    }
  }

  async scrapeSeries(args: ScraperArgs): Promise<MediaSource[]> {
    const { episodeIndex, seasonIndex, title } = args;
    if (
      title != null && seasonIndex != null &&
      episodeIndex != null
    ) {
      let episodes: DirObject[] = [];

      // recursive function
      const findEpisodes = async (
        nodes: DirObject[],
      ): Promise<DirObject[]> => {
        let episodes: DirObject[] = [];
        for (const node of nodes) {
          if (isSeriesFile(node.meta)) {
            // check if episode number matches
            if (
              node.meta?.episodeNumbers.includes(episodeIndex) ||
              (node.meta.complete ||
                  node.meta.fullSeason) && !node.meta.isSeasonExtra
            ) {
              if (
                node.meta?.seasons.length > 0 &&
                !node.meta?.seasons.includes(seasonIndex)
              ) {
                // if filename also has season number
                // and it doesn't match then skip
                log.debug("Season number does not match");
                continue;
              }
              if (
                node.mimeType != null &&
                (node.mimeType.includes("video") ||
                  node.mimeType.includes("mpegURL"))
              ) {
                // add episode if extension matches
                log.debug(`Adding file ${node.path} to result`);
                episodes.push(node);
              }
              continue;
            }
          } else if (isFolder(node.meta)) {
            if (
              ["Hindi", "English"].includes(node.name) ||
              node.meta?.seasons?.includes(seasonIndex) ||
              node.meta?.resolution != null
            ) {
              if (
                node.meta?.seasons != null && node.meta?.seasons?.length > 0 &&
                !node.meta?.seasons.includes(seasonIndex)
              ) {
                // if folder also has season number
                // and it doesn't match then skip
                log.debug("Season number does not match, continuing");
                continue;
              }
              // if folder is resolution or matches season
              // then dive deeper
              log.debug("Diving into folder named " + node.name);
              const nodes = await this.nodeTree.getNodes(node.path, true);
              const nestedNodes = await findEpisodes(nodes);
              episodes = episodes.concat(nestedNodes);
            }
          }
        }
        return episodes;
      };

      for (const arg of this.nodeTreePaths.series) {
        const seriesDirs = await this.filterDirs(arg, title);

        for (const dir of seriesDirs) {
          log.debug("Diving into folder named " + dir.name);
          const nodes = await this.nodeTree.getNodes(dir.path, true);
          const deepEpisodes = await findEpisodes(nodes);

          episodes = episodes.concat(deepEpisodes);
        }
      }

      const sources = episodes.map<MediaSource | null>(
        (episode, index) => {
          const mimeType = episode.mimeType;
          // const type = mimeType?.split("/")[0];

          if (
            mimeType != null &&
            !(mimeType.includes("video") || mimeType.includes("mpegURL"))
          ) {
            return null;
          }

          const { resolution, videoCodec, sources } = episode
            .meta as ParsedFilename;
          const displayName = [
            resolution,
            videoCodec,
            sources[0],
          ]
            .filter((v) => v != null)
            .join(" ").trim();

          return {
            streamUri: this.urlTransformer != null
              ? this.urlTransformer(episode.path).toString()
              : episode.path.toString(),
            fileName: episode.name,
            fileSize: episode.fileSize ?? undefined,
            mimeType: mimeType,
            displayName: (displayName != null && displayName !== "")
              ? displayName
              : episode.lastModified != null
              ? (`Added on ${episode.lastModified.toLocaleDateString()}`)
              : `Source #${index + 1}`,
          };
        },
      ).filter((v) => v != null) as MediaSource[];

      if (sources.length === 0) {
        throw createHttpError(404, "No sources found from " + this.name + " (" + this.nodeTree.name + ")");
      }

      return sources;
    } else {
      throw createHttpError(400, `Invalid args passed to ${this.name}`);
    }
  }

  scrapeSources(
    args: ScraperArgs,
  ): Promise<MediaSource[]> {
    if (args.isSeries) {
      return this.scrapeSeries(args);
    } else {
      return this.scrapeMovies(args);
    }
  }
}
