import {
  completeRange,
  parseEdition,
  parseResolution,
  parseSource,
} from "video-filename-parser";
import { ParsedFolder } from "models/node-tree-builder.ts";

const tvTitleRegexes = [
  // Matches: Batwoman (TV Series 2019-) 720p
  /(?<title>^(.*?))\s?[\(]/i,
  // Matches: Two Broke Girls - 2011 - 720p
  /^(?<title>^(.*?))\s-\s/i,
  // Matches The Insurgent 2015 720p
  /(?<title>^.*?)\s(?:1(8|9)|20)\d{2}/i,
  // Matches The.Marvelous.Mrs..Maisel.2017
  /(?<title>^.+)\.(?:1(8|9)|20)\d{2}/i,
];
const tvYearRegexes = [/(?<year>(1(8|9)|20)\d{2})/i];
const tvDubbingRegexes = [
  /(?<dubbing>(hindi|bangla|urdu|bengali))\sdub(bed|bing)?/i,
];
const tvSeasonRegexes = [
  /Seasons?\s(?<lowest>\d{1,2})\s?-\s?(?<highest>\d{1,2})/i,
  /Seasons?\s(?<season>\d{1,2})/i,
];

export function parseTitle(name: string): string | undefined {
  for (const exp of tvTitleRegexes) {
    const match = exp.exec(name);
    if (match?.groups) {
      if (match?.groups.title == null) continue;
      return match.groups.title;
    }
  }
}

export function parseYear(name: string): string | undefined {
  for (const exp of tvYearRegexes) {
    const match = exp.exec(name);
    if (match?.groups) {
      if (match?.groups.year == null) continue;
      return match.groups.year;
    }
  }
}

export function parseDubbing(name: string): string | undefined {
  for (const exp of tvDubbingRegexes) {
    const match = exp.exec(name);
    if (match?.groups) {
      if (match?.groups.dubbing == null) continue;
      return match.groups.dubbing;
    }
  }
}
export function parseSeasons(name: string): number[] | undefined {
  for (const exp of tvSeasonRegexes) {
    const match = exp.exec(name);
    if (match?.groups) {
      if (match?.groups?.season != null) {
        return [Number(match.groups.season)];
      } else if (
        match?.groups?.lowest != null && match?.groups?.highest != null
      ) {
        const lowest = Number(match.groups.lowest);
        const highest = Number(match.groups.highest);
        return completeRange([lowest, highest]);
      } else continue;
    }
  }
}

export function folderParse(name: string): ParsedFolder {
  const title = parseTitle(name);
  const year = parseYear(name);
  const dubbing = parseDubbing(name);
  const { resolution } = parseResolution(name);
  const seasons = parseSeasons(name);
  const sources = parseSource(name);
  const edition = parseEdition(name);

  return {
    title: seasons != null ? undefined : title,
    year,
    seasons,
    dubbing,
    resolution,
    sources,
    edition,
    isFolder: true,
  };
}
