/// <reference types="../../types/types.d.ts" />

import { createHttpError } from "oak/deps.ts";
import { typeByExtension } from "@std/media-types";
import { extname } from "@std/path";
import {
  Cast,
  MediaSource,
  Network,
  SearchResult,
} from "models/media/common.ts";
import { Movie } from "models/media/movie.ts";
import { Episode, Season, Series } from "models/media/series.ts";
import { Parser } from "models/parser.ts";
import { mapEmbyPathToMediaHost, mapToImageUris } from "./helpers.ts";

export class EmbyParser implements Parser {
  parseMediaSources(
    sources: dynamic,
  ): MediaSource[] {
    if (sources != null && Array.isArray(sources) && sources.length !== 0) {
      return sources.map<MediaSource>((source) => {
        const streams = (source["MediaStreams"] as dynamic[]).filter(
          (stream) => stream["Type"] === "Video",
        );
        const streamUri = encodeURI(mapEmbyPathToMediaHost(source["Path"]));
        return {
          streamUri,
          bitrate: Number(source["Bitrate"]),
          fileName: source["Name"],
          fileSize: Number(source["Size"]),
          mimeType: typeByExtension(extname(streamUri)),
          displayName: streams[0]["DisplayTitle"],
        };
      });
    } else throw createHttpError(404, "No sources found for EmbyEngine");
  }

  parseMovie(
    json: dynamic,
  ): Movie {
    if (json["Type"] === "Movie") {
      const movie: Movie = {
        id: json["Id"],
        title: json["Name"],
        year: json["ProductionYear"],
        synopsis: json["Overview"],
        directors: (json["People"] as dynamic[])?.filter((v) =>
          v["Type"] === "Director"
        )?.map((v) => v["Name"]),
        runtime: (json["RunTimeTicks"] as number) / 10000,
        externalIds: {
          imdb: json["ProviderIds"]["Imdb"],
          tmdb: json["ProviderIds"]["Tmdb"],
        },
        genres: json["Genres"],
        ageRating: json["OfficialRating"],
        criticRatings: {
          rottenTomatoes: json["CriticRating"],
        },
        imageUris: mapToImageUris(
          json["ImageTags"],
          json["BackdropImageTags"],
        )(json["Id"]),
        cast: this.parseCast(json),
        adult: false,
      };
      return movie;
    } else {
      throw createHttpError(400, "Not an id of a movie");
    }
  }

  parseSeries(
    json: dynamic,
  ): Series {
    if (json["Type"] === "Series") {
      const series: Series = <Series> {
        id: json["Id"],
        title: json["Name"],
        year: json["ProductionYear"],
        synopsis: json["Overview"],
        averageRuntime: json["RunTimeTicks"] != null
          ? (json["RunTimeTicks"] as number) / 10000
          : undefined,
        providerIds: {
          imdb: json["ProviderIds"]["Imdb"],
          tmdb: json["ProviderIds"]["Tmdb"],
        },
        criticRatings: {
          community: json["CommunityRating"],
        },
        genres: json["Genres"],
        ageRating: json["OfficialRating"],
        endDate: json["EndDate"] != null
          ? new Date(json["EndDate"])
          : undefined,
        hasEnded: json["Status"] != null
          ? json["Status"] === "Ended"
          : undefined,
        networks: (json["Studios"] as dynamic[]).map<Network>((v) => ({
          id: v["Id"],
          name: v["Name"],
        })),
        // cast: (dynamic["People"] as dynamic[]).filter((v) => v["Type"] === "Actor")
        //   .map<Cast>((v) => ({ name: v["Name"], role: v["Role"] })),
        imageUris: mapToImageUris(
          json["ImageTags"],
          json["BackdropImageTags"],
        )(json["Id"]),
        cast: this.parseCast(json),
        childCount: json["ChildCount"],
        adult: false,
      };
      return (series);
    } else throw (createHttpError(400, "Not a series"));
  }

  parseResult(
    json: dynamic,
  ): SearchResult {
    return {
      id: json["Id"],
      name: json["Name"],
      imageUris: mapToImageUris(
        json["ImageTags"],
        json["BackdropImageTags"],
      )(json["Id"]),
      year: json["Year"],
      adult: false,
      isMovie: json["Type"] === "Movie"
        ? true
        : json["Type"] === "Series"
        ? false
        : false,
    };
  }

  parseSeason(
    json: dynamic,
  ): Season {
    if (
      json["Type"] === "Season"
    ) {
      return {
        id: json["Id"],
        seriesId: json["SeriesId"],
        index: json["IndexNumber"],
        name: json["Name"],
        childCount: json["ChildCount"],
        imageUris: mapToImageUris(json["ImageTags"])(json["Id"]),
      };
    } else throw (createHttpError(400, "Not a season"));
  }

  parseSeasons(
    json: dynamic,
  ): Season[] {
    if (
      json["Items"] != null &&
      Array.isArray(json["Items"])
    ) {
      if (json["Items"].length !== 0) {
        return (
          json["Items"].map<Season>(this.parseSeason)
        );
      } else {
        throw createHttpError(404, "No seasons found");
      }
    } else throw createHttpError(500, "Could not parse seasons");
  }

  parseEpisode(
    json: dynamic,
  ): Episode {
    if (
      json["Type"] === "Episode"
    ) {
      return {
        id: json["Id"],
        seriesId: json["SeriesId"],
        seasonIndex: json["SeasonId"],
        name: json["Name"],
        index: json["IndexNumber"],
        synopsis: json["Overview"],
        directors: (json["People"] as dynamic[])?.filter((v) =>
          v["Type"] === "Director"
        )?.map((v) => v["Name"]),
        runtime: Number(json["RunTimeTicks"] / 10000),
        airDate: json["PremiereDate"] != null
          ? new Date(json["PremiereDate"])
          : undefined,
        imageUris: mapToImageUris(json["ImageTags"])(json["Id"]),
      };
    } else {
      throw createHttpError(400, "Not an episode");
    }
  }

  parseEpisodes(
    json: dynamic,
  ): Episode[] {
    if (
      json["Items"] != null &&
      Array.isArray(json["Items"])
    ) {
      if (json["Items"].length !== 0) {
        const items = json["Items"].map<Episode>(this.parseEpisode);
        return items;
      } else throw createHttpError(404, "No episodes available");
    } else {
      throw createHttpError(500, "Could not parse episodes");
    }
  }

  parseCast(json: dynamic): Cast[] {
    if (Array.isArray(json["People"])) {
      return json["People"]
        .filter((v: dynamic) => v["Type"] === "Actor")
        .map<Cast>((v) => ({
          id: v["Id"],
          name: v["Name"],
          role: v["Role"],
          imageUris: mapToImageUris({
            "Primary": v["PrimaryImageTag"],
          })(v["Id"]),
        }));
    } else {
      throw createHttpError(500, "Error parsing people");
    }
  }
}
