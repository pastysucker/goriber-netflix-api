import { createHttpError } from "oak/deps.ts";
import { ImageUris } from "models/media/common.ts";
import { baseUrl } from "providers/emby/constants.ts";

export const mapHostPathToEmbyPath = (
  hostPath: string,
  type: string,
): string => {
  const url = new URL(hostPath);
  switch (type) {
    case "englishSeries":
      return ("/bnet-server-4/FILE--SERVER" + url.pathname);
    case "englishMovies":
      return ("/bnet-server-2/FILE--SERVER" + url.pathname);
    default:
      throw createHttpError(500, "Unknown type of media");
  }
};

export const mapEmbyPathToMediaHost = (embyPath: string): string => {
  return embyPath
    .replace(/\/bnet-server-1\/FILE--SERVER/, "http://server1.ftpbd.net")
    .replace(/\/bnet-server-2\/FILE--SERVER/, "http://server2.ftpbd.net")
    .replace(/\/bnet-server-3\/FILE--SERVER/, "http://server3.ftpbd.net")
    .replace(/\/storage-pool\/FILE--SERVER/, "http://server3.ftpbd.net")
    .replace(/\/bnet-server-4\/FILE--SERVER/, "http://server4.ftpbd.net")
    .replace(/\/bnet-server-6\/FILE--SERVER/, "http://server4.ftpbd.net")
    .replace(/\/var\/www\/html\/FILE--SERVER/, "http://server5.ftpbd.net");
};

enum ImageQueries {
  Primary = "quality=60&maxHeight=500",
  Backdrop = "quality=50&maxHeight=1080",
  Thumb = "quality=50&maxHeight=500",
  Logo = "quality=50&maxWidth=300",
  Banner = "quality=70&maxWidth=800",
}

const buildImageUrl = (
  id: string,
  imageType: "Primary" | "Backdrop" | "Thumb" | "Logo" | "Banner",
  imageTag: string,
) =>
  `${baseUrl}/emby/Items/${id}/Images/${imageType}?tag=${imageTag}&${
    ImageQueries[imageType]
  }`;

export const mapToImageUris = (
  imageTags: Record<string, string>,
  backdropTags?: string[],
) =>
(id: string): ImageUris => {
  const imageUris: ImageUris = {};

  if (Object.prototype.hasOwnProperty.call(imageTags, "Primary")) {
    imageUris.primary = buildImageUrl(id, "Primary", imageTags["Primary"]);
  }
  if (backdropTags != null && backdropTags.length !== 0) {
    imageUris.backdrop = buildImageUrl(id, "Backdrop", backdropTags[0]);
  }
  if (Object.prototype.hasOwnProperty.call(imageTags, "Thumb")) {
    imageUris.thumb = buildImageUrl(id, "Thumb", imageTags["Thumb"]);
  }
  if (Object.prototype.hasOwnProperty.call(imageTags, "Logo")) {
    imageUris.logo = buildImageUrl(id, "Logo", imageTags["Logo"]);
  }
  if (Object.prototype.hasOwnProperty.call(imageTags, "Banner")) {
    imageUris.banner = buildImageUrl(id, "Banner", imageTags["Banner"]);
  }

  return imageUris;
};
