/// <reference types="../../types/types.d.ts"/>

import { createHttpError } from "oak/deps.ts";
import { headers } from "./constants.ts";
import log from "logger";

export const fetcher = (
  url: string,
  queries?: Map<string, string>,
): Promise<dynamic> => {
  const controller = new AbortController();
  const timeout = setTimeout(() => {
    controller.abort();
  }, 15000); // will time out after 15000ms

  const finalUrl = url +
    (queries != null
      ? "?" + new URLSearchParams(queries as dynamic).toString()
      : "");

  log.debug("Making request to Emby server");

  return fetch(finalUrl, { headers, signal: controller.signal }).then((res) => {
    if (res.ok) {
      return res;
    } else {
      res.text().then((v) => log.error(v));
      throw createHttpError(
        502,
        "media.ftpbd.net:8096 is having problems",
      );
    }
  })
    .then((res) => res.json())
    .catch((e) => {
      if (e instanceof Error) {
        throw createHttpError(502, e.message);
      } else throw e;
    }).finally(() => {
      clearTimeout(timeout);
    });
};
