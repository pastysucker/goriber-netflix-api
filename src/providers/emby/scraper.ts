import { MediaSource } from "models/media/common.ts";
import { Scraper, ScraperArgs } from "models/scraper.ts";
import { baseUrl } from "./constants.ts";
import { fetcher } from "./client.ts";
import { EmbyEngine } from "./engine.ts";
import { createHttpError } from "oak/deps.ts";
import log from "shared/logger.ts";

export class EmbyScraper implements Scraper {
  readonly engine: EmbyEngine;
  readonly name = "EmbyScraper";
  readonly nsfw = false;

  constructor(engine: EmbyEngine) {
    this.engine = engine;
  }

  private async getIds(
    externalIds: Pick<ScraperArgs, "imdbId" | "tvdbId">,
    isSeries: boolean,
  ): Promise<string[]> {
    const json = await fetcher(
      `${baseUrl}/emby/Items`,
      new Map([
        ["EnableUserData", "false"],
        ["Recursive", "true"],
        ["Fields", "ProductionYear"],
        ["EnableImages", "false"],
        [
          "AnyProviderIdEquals",
          (externalIds.imdbId != null ? `imdb.${externalIds.imdbId}` : "") +
          (externalIds.tvdbId != null
            ? ("," + `tvdb.${externalIds.tvdbId}`)
            : ""),
        ],
      ]),
    );
    const ids = (json["Items"] as dynamic[])
      .filter((v) => v["Type"] === (isSeries ? "Episode" : "Movie"))
      .map<string>((v) => v["Id"]);
    return ids;
  }

  async scrapeSources(
    args: ScraperArgs,
  ): Promise<MediaSource[]> {
    if (args.imdbId != null || args.tvdbId != null) {
      const ids = await this.getIds(args, args.isSeries);
      if (args.isSeries) {
        const id: string | undefined = ids[0];
        if (id == null) {
          throw createHttpError(404, "No sources found from " + this.name);
        }
        log.debug(`Found episode file with id ${id}`);
        const info = await this.engine.getPlaybackInfo(id);
        return info;
      } else {
        const promises = ids.map(async (id) => {
          log.debug(`Found movie file with id ${id}`);
          return await this.engine.getPlaybackInfo(id);
        });
        const resolved = await Promise.all(promises).then((v) => v.flat());
        return resolved;
      }
    } else {
      throw createHttpError(404, "No provider ids found associated with media");
    }
  }
}
