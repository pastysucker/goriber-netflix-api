import {
  _seriesEndpoint,
  _userEndpoint,
  baseUrl,
  CollectionId,
  userId,
} from "providers/emby/constants.ts";
import ItemType from "types/itemType.ts";
import { Engine } from "models/engine.ts";
import { MediaSource, SearchResult } from "models/media/common.ts";
import { EmbyParser } from "providers/emby/parser.ts";
import { fetcher } from "providers/emby/client.ts";
import { noop } from "shared/utils.ts";

export class EmbyEngine implements Engine {
  readonly parser: EmbyParser;

  constructor(embyParser: EmbyParser) {
    this.parser = embyParser;
  }

  getRawItem(id: string): Promise<Record<string, unknown>> {
    return fetcher(
      `${_userEndpoint}/Items/${id}`,
      new Map([
        ["EnableUserData", "false"],
        ["Fields", "Path"],
      ]),
    );
  }

  searchItems(
    query: string,
    itemType: ItemType,
    limit = 6,
    skip = 0,
  ): Promise<SearchResult[]> {
    return fetcher(
      `${_userEndpoint}/Items`,
      new Map([
        ["searchTerm", query],
        ["Fields", "ProductionYear,Status"],
        ["IncludeItemTypes", itemType],
        ["IncludePeople", "false"],
        ["EnableUserData", "false"],
        ["Limit", String(limit)],
        ["StartIndex", String(skip)],
        ["Recursive", "true"],
      ]),
    )
      .then((json: dynamic) =>
        (json["Items"] as dynamic[]).map<SearchResult>((v) =>
          this.parser.parseResult(v)
        )
      );
  }

  discoverItems(
    _itemType: ItemType,
  ): Promise<SearchResult[]> {
    return fetcher(
      `${_userEndpoint}/Items/Latest`,
      new Map([
        ["Fields", "ProductionYear"],
        // ["Limit", String(limit)],
        // ["StartIndex", String(skip)],
        ["ParentId", CollectionId.EnglishMovies],
        //[ IncludeItemTypes: itemType],
        ["EnableUserData", "false"],
        ["Recursive", "true"],
      ]),
    )
      .then((json: dynamic[]) => json.map(this.parser.parseResult));
  }

  getSimilarItems(
    itemId: string,
  ): Promise<SearchResult[]> {
    return fetcher(
      `${baseUrl}/emby/Items/${itemId}/Similar`,
      new Map([
        // ["Limit", String(limit)],
        ["Fields", "ProductionYear"],
        ["EnableUserData", "false"],
        ["UserId", userId],
      ]),
    )
      .then((json: dynamic) =>
        (json["Items"] as dynamic[]).map(this.parser.parseResult)
      );
  }

  getPlaybackInfo(
    id: string,
  ): Promise<MediaSource[]> {
    return fetcher(`${baseUrl}/emby/Items/${id}/PlaybackInfo`)
      .then((json: dynamic) =>
        this.parser.parseMediaSources(json["MediaSources"])
      );
  }

  getMovie(_id: string) {
    return noop();
  }

  getEpisode(_seriesId: string, _seasonIndex: number, _episodeIndex: number) {
    return noop();
  }

  getEpisodes(_seriesId: string, _seasonIndex: number) {
    return noop();
  }

  getSeries(_seriesId: string) {
    return noop();
  }

  getSeason(_seriesId: string, _seasonIndex: number) {
    return noop();
  }

  getSeasons(_seriesId: string) {
    return noop();
  }
}
