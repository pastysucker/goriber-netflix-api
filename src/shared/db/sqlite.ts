/// <reference types="../../types/types.d.ts" />

import { DB, Row } from "sqlite/mod.ts";
import { Sql, sqlite } from "polysql/mod.ts";
import { DEFAULT_SETTINGS_SQLITE } from "polysql/sql_settings.ts";
import { getDataDir, initDirs } from "shared/config.ts";
import log from "logger";

const { dataLocalDir } = initDirs();
try {
  Deno.statSync(dataLocalDir);
} catch (e) {
  if (e["name"] == "NotFound") {
    Deno.mkdirSync(dataLocalDir, { recursive: true });
    log.info("Initialized data directory");
  }
}

const databasePath = getDataDir() + "/primary.db";

export const db = new DB(databasePath, { mode: "create" });

db.query(`PRAGMA foreign_keys = ON`);

db.query(
  sqlite`
  CREATE TABLE IF NOT EXISTS
  users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL UNIQUE,
    admin BOOLEAN NOT NULL CHECK(admin IN (0, 1)) DEFAULT 0
  )
  `.toString(),
);

db.query(
  sqlite`
  CREATE TABLE IF NOT EXISTS
  media (
    id TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    year INTEGER,
    media_type TEXT NOT NULL CHECK(media_type IN ('movie', 'series', 'episode')),
    i_primary TEXT,
    i_backdrop TEXT,
    i_thumb TEXT,
    i_logo TEXT,
    i_banner TEXT
  )`.toString(),
);

db.query(
  sqlite`
  CREATE TABLE IF NOT EXISTS
  favorites (
    media_id TEXT NOT NULL,
    user_id INTEGER NOT NULL,
    PRIMARY KEY (media_id, user_id),
    FOREIGN KEY(media_id) REFERENCES media(id) ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
  )
`.toString(),
);

db.query(
  sqlite`
  CREATE TABLE IF NOT EXISTS
  nextup (
    created TEXT DEFAULT CURRENT_DATE,
    series_id TEXT NOT NULL,
    season_index INTEGER NOT NULL,
    episode_index INTEGER NOT NULL,
    episode_id TEXT NOT NULL,
    user_id INTEGER NOT NULL,
    PRIMARY KEY (series_id, user_id),
    FOREIGN KEY(series_id) REFERENCES media(id) ON DELETE CASCADE,
    FOREIGN KEY(episode_id) REFERENCES media(id) ON DELETE CASCADE,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
  )`.toString(),
);

db.query(
  sqlite`
  CREATE TABLE IF NOT EXISTS
  cache (
    key TEXT PRIMARY KEY,
    policy_base64 TEXT,
    body_base64 TEXT,
    created TEXT DEFAULT CURRENT_TIMESTAMP
  )
  `.toString(),
);

db.query(
  sqlite`
  CREATE TABLE IF NOT EXISTS
  trakt (
    id INTEGER PRIMARY KEY,
    user_id INTEGER NOT NULL UNIQUE,
    access_token TEXT NOT NULL UNIQUE,
    refresh_token TEXT NOT NULL UNIQUE,
    expires_in INTEGER NOT NULL DEFAULT 7200,
    scope TEXT,
    token_type TEXT,
    created_at INTEGER,
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
  )
  `.toString(),
);

// Seed
db.query(
  sqlite`
  INSERT INTO users (id, username, admin)
  SELECT 1, 'Tamim Arafat', 1
  WHERE NOT EXISTS(SELECT 1 FROM users WHERE id = 1 AND username = 'Tamim Arafat')
`.toString(),
);

export function query<R extends Row = Row>(
  strings: TemplateStringsArray,
  ...params: dynamic[]
) {
  const sql = new Sql(DEFAULT_SETTINGS_SQLITE, [...strings], params);
  const sqlString: string = (sql as dynamic).strings.join("?").trim();
  const sqlParams: dynamic[] = (sql as dynamic).params;
  return db.query<R>(sqlString, sqlParams);
}
