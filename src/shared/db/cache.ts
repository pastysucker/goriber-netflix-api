import { Cache } from "httpcache/mod.ts";
import { db } from "shared/db/sqlite.ts";
import { decodeBase64, encodeBase64 } from "@std/encoding";

export function sqliteCache(ttl = 28800): Cache {
  return new Cache({
    get(url) {
      const [result] = db.query<[string, string]>(
        `SELECT policy_base64, body_base64 FROM cache WHERE key = ?`,
        [url],
      );
      if (result != null) {
        const [policyBase64, bodyBase64] = result;
        const policy = JSON.parse(atob(policyBase64));
        const body = decodeBase64(bodyBase64);
        return Promise.resolve({ policy, body });
      } else return Promise.resolve(undefined);
    },
    set(url, resp) {
      db.query(
        `DELETE FROM cache WHERE strftime('%s', 'now') - strftime('%s', created) > ?`,
        [ttl],
      );

      const policyBase64 = btoa(JSON.stringify(resp.policy));
      const bodyBase64 = encodeBase64(resp.body);
      db.query(
        `INSERT OR REPLACE INTO cache (key, policy_base64, body_base64) VALUES (?, ?, ?)`,
        [url, policyBase64, bodyBase64],
      );
      return Promise.resolve();
    },
    close() {
      return Promise.resolve();
    },
    delete(url) {
      db.query(`DELETE FROM cache WHERE key = ?`, [url]);
      return Promise.resolve();
    },
  });
}
