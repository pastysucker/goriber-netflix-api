import { interpolateYears, isFile, isYearInRange } from "../utils.ts";
import { assertEquals } from "std/testing/asserts.ts";

Deno.test("should assert files correctly", () => {
  const cases: [string, boolean][] = [
    ["Zoey%27s.Extraordinary.Playlist.2020", false],
    ["Watchmen_1x4.m3u8", true],
    ["playlist.m3u", true],
    ["see.s02e06.1080p.web.h264-cakes.mkv", true],
  ];
  for (const [test, expected] of cases) {
    const bool = isFile(test);
    assertEquals(bool, expected);
  }
});

Deno.test("should correctly interpolate years", () => {
  const cases: [number, number][][] = [
    [[0, 1949], [1900, 1949]],
    [[1950, 69], [1950, 1969]],
    [[1990, 99], [1990, 1999]],
    [[2000, 9], [2000, 2009]],
    [[2010, 18], [2010, 2018]],
    [[0, 1999], [1900, 1999]],
  ];
  for (const [test, expected] of cases) {
    assertEquals(interpolateYears(...test), expected);
  }
});

Deno.test("should correctly determine if year is in range", () => {
  const cases: [[year: number, start: number, end: number], boolean][] = [
    [[1950, 0, 1949], false],
    [[2010, 2000, 9], false],
    [[1936, 1920, 55], true],
  ];
  for (const [test, expected] of cases) {
    assertEquals(isYearInRange(...test), expected);
  }
});
