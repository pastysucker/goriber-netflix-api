import { Resolution } from "video-filename-parser";
import { assertEquals, assertThrows } from "std/testing/asserts.ts";
import { MoreResolution, parseMasterPlaylist } from "../mod.ts";

const MASTER_PLAYLIST = `#EXTM3U
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=4500000, RESOLUTION=1920x1080
H4/v.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=1800000, RESOLUTION=1280x720
H3/v.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=1200000, RESOLUTION=854x480
H2/v.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=720000, RESOLUTION=640x360
H1/v.m3u8`;

const CHILD_PLAYLIST = `#EXTM3U
#EXT-X-VERSION:3
#EXT-X-TARGETDURATION:5
#EXT-X-MEDIA-SEQUENCE:1
#EXTINF:4.046444,
skate_phantom_flex_4k_228_144p1.ts
#EXT-X-ENDLIST`;

Deno.test("correctly parses master playlist", () => {
  const playlist = parseMasterPlaylist(MASTER_PLAYLIST);
  assertEquals(playlist, {
    sections: [
      { path: "H4/v.m3u8", bandwidth: 4500000, resolution: Resolution.R1080P },
      { path: "H3/v.m3u8", bandwidth: 1800000, resolution: Resolution.R720P },
      { path: "H2/v.m3u8", bandwidth: 1200000, resolution: Resolution.R480P },
      {
        path: "H1/v.m3u8",
        bandwidth: 720000,
        resolution: MoreResolution.R360P,
      },
    ],
  });
});

Deno.test("fails to parse child playlist", () => {
  assertThrows(() => parseMasterPlaylist(CHILD_PLAYLIST));
});
