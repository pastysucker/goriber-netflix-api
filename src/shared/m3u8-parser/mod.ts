import { Resolution } from "video-filename-parser";

export enum MoreResolution {
  R144P = "144P",
  R240P = "240P",
  R360P = "360P",
  R1440P = "1440P",
}

export type ExtendedResolution = Resolution | MoreResolution;

export type Section = {
  bandwidth?: number;
  resolution?: ExtendedResolution;
  fullUri?: URL;
  path: string;
};

export interface Playlist {
  sections: Section[];
}

export function parseDimensions(str: string): ExtendedResolution {
  const [_, height] = str.split("x");
  const keys = [...Object.keys(Resolution), ...Object.keys(MoreResolution)];
  const keyIndex = keys.indexOf("R" + height + "P");
  if (keyIndex < Object.keys(Resolution).length) {
    return Resolution[keys[keyIndex] as keyof typeof Resolution];
  } else {
    return MoreResolution[keys[keyIndex] as keyof typeof MoreResolution];
  }
}

export function parseMasterPlaylist(raw: string, baseUrl?: string): Playlist {
  const lines = raw.split("\n");

  if (lines[0] !== "#EXTM3U") {
    throw new Error("Invalid playlist!");
  }
  // if (lines[1].startsWith("#EXT-X-VERSION")) {
  //   throw new Error("Not a master playlist!");
  // } // NOTE: temporarily disable master playlist check until more reliable way is implemented

  const sections: Section[] = [];

  for (let i = 0; i < lines.length; i++) {
    if (lines[i].startsWith("#EXT-X-STREAM-INF")) {
      // match key-value metadata pairs
      const kvRegex = new RegExp(
        "(?<pair>(?<key>[^#:]+?)(?:=)(?<value>[^=]+)(?:,|$))",
        "g",
      );
      let matches: RegExpExecArray | null;
      const section: Section = { path: "" };
      while ((matches = kvRegex.exec(lines[i])) != null) {
        const groups = matches.groups!;
        if (groups["key"].trim() === "BANDWIDTH") {
          section.bandwidth = parseInt(groups["value"]);
        }
        if (groups["key"].trim() === "RESOLUTION") {
          section.resolution = parseDimensions(groups["value"]);
        }
      }
      section.path = lines[i + 1];
      if (baseUrl != null) {
        section.fullUri = new URL(section.path, baseUrl);
      }
      sections.push(section);
    }
  }

  return { sections };
}
