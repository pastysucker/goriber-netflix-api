import { ConsoleStream, Level, Logger } from "optic/mod.ts";
import { TokenReplacer } from "optic/formatters/mod.ts";

const consoleStream = new ConsoleStream()
  .withMinLogLevel(Level.Debug)
  .withFormat(
    new TokenReplacer()
      .withFormat("{dateTime} [{level}] {msg}")
      .withDateTimeFormat("h:mm:ss YYYY-MM-DD")
      .withColor()
      .withLevelPadding(0),
  );

consoleStream.outputHeader = false;
consoleStream.outputFooter = false;

const logger = new Logger();
logger
  .withMinLogLevel(Level.Debug)
  .addStream(consoleStream);

export default logger;
