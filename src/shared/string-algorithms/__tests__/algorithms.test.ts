import statcan from "../statcan.ts";
import dice from "../dice-coefficient.ts";
import { assertEquals, assertNotEquals } from "std/testing/asserts.ts";

Deno.test("should assert that two similar sounding words are not equal", () => {
  const s1 = statcan("Alias");
  const s2 = statcan("Alice");

  assertNotEquals(s1, s2);
});

Deno.test("should get correct dice coefficient for two strings", () => {
  const coef = dice("hat", "mat");
  assertEquals(coef, 0.5);
});
