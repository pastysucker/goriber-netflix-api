// @ts-nocheck
// deno-lint-ignore-file

export default function diceCoefficent(fst: string, snd: string) {
  var i, j, k, map, match, ref, ref1, sub;
  if (fst.length < 2 || fst.length < 2) {
    return 0;
  }
  map = new Map();
  for (
    i = j = 0, ref = fst.length - 2;
    0 <= ref ? j <= ref : j >= ref;
    i = 0 <= ref ? ++j : --j
  ) {
    sub = fst.substring(i, 2);
    if (map.has(sub)) {
      map.set(sub, map.get(sub) + 1);
    } else {
      map.set(sub, 1);
    }
  }
  match = 0;
  for (
    i = k = 0, ref1 = snd.length - 2;
    0 <= ref1 ? k <= ref1 : k >= ref1;
    i = 0 <= ref1 ? ++k : --k
  ) {
    sub = snd.substring(i, 2);
    if (map.get(sub) > 0) {
      match++;
      map.set(sub, map.get(sub) - 1);
    }
  }
  return (2.0 * match) / (fst.length + snd.length - 2);
}
