import { TMDbParser } from "providers/tmdb/parser.ts";
import { TMDbEngine } from "providers/tmdb/engine.ts";
import { EmbyEngine } from "providers/emby/engine.ts";
import { EmbyParser } from "providers/emby/parser.ts";
import { EmbyScraper } from "providers/emby/scraper.ts";
import { NodeTreeScraper } from "providers/node-tree/scraper.ts";
import { H5aiNodeBuilder } from "providers/h5ai/node-builder.ts";
import { NginxNodeBuilder } from "providers/nginx/node-builder.ts";
import { FileSystemNodeBuilder } from "providers/filesystem/node-builder.ts";
import { PerverzijaScraper } from "providers/perverzija/scraper.ts";
import { PerverzijaEngine } from "providers/perverzija/engine.ts";
import { SflixScraper } from "providers/sflix/scraper.ts";
import * as path from "@std/path";
import { readConfigFile } from "shared/config.ts";

const { scrapers: { fileSystem, h5ai, nginx } } = await readConfigFile();

export const tmdbParser = new TMDbParser();
export const tmdbEngine = new TMDbEngine(tmdbParser);
export const embyParser = new EmbyParser();
export const embyEngine = new EmbyEngine(embyParser);
export const embyScraper = new EmbyScraper(embyEngine);
export const sflixScraper = new SflixScraper()
export const perverzijaEngine = new PerverzijaEngine();
export const perverzijaScraper = new PerverzijaScraper(perverzijaEngine);
export const h5aiScraper = new NodeTreeScraper(
  new H5aiNodeBuilder(),
  {
    movie: h5ai.movies
      .map((v) => new URL(v)),
    series: h5ai.series
      .map((v) => new URL(v)),
  },
);
export const nginxScraper = new NodeTreeScraper(
  new NginxNodeBuilder(),
  {
    series: nginx.series.map((v) => new URL(v)),
    movie: nginx.movies.map((v) => new URL(v)),
  },
);
export const fileSystemScraper = new NodeTreeScraper(
  new FileSystemNodeBuilder(),
  {
    movie: fileSystem.movies.map((v) => new URL("file://" + v)),
    series: fileSystem.series.map((v) => new URL("file://" + v)),
  },
  {
    urlTransformer: (url) =>
      new URL(
        fileSystem.fileServerUrl + "/" +
          path.relative(fileSystem.rootMediaDirectory, url.pathname),
      ),
  },
);
