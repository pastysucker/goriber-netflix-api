import { projectDirs } from "directories";
import { deepmerge } from "deepmerge-ts";
import * as fs from "@std/fs";
import * as dotenv from "@std/dotenv";
import * as YAML from "@std/yaml";

type Config = {
  scrapers: {
    enabled: string[];
    h5ai: {
      series: string[];
      movies: string[];
    };
    nginx: {
      series: string[];
      movies: string[];
    };
    fileSystem: {
      fileServerUrl: string;
      rootMediaDirectory: string;
      series: string[];
      movies: string[];
    };
  };
  env: {
    envFile: string;
  };
};

const defaultConfig: Config = {
  scrapers: {
    enabled: [],
    fileSystem: {
      movies: [],
      series: [],
      fileServerUrl: "http://fileserver:6085",
      rootMediaDirectory: "/media",
    },
    h5ai: {
      movies: [],
      series: [],
    },
    nginx: {
      series: [],
      movies: [],
    },
  },
  env: {
    envFile: "/home/user/.local/share/goribernetflix/primary.env",
  },
};

export function initDirs() {
  const { configDir, cacheDir, dataLocalDir } = projectDirs.setup(
    "com",
    "arafatamim",
    "goribernetflix",
  );
  return {
    configDir,
    cacheDir,
    dataLocalDir,
  };
}

export function getConfigDir(): string {
  const { configDir: localConfig } = initDirs();
  const configDir = Deno.env.get("CONFIG_DIR") ?? localConfig;

  return configDir;
}

export function getDataDir(): string {
  const { dataLocalDir } = initDirs();
  const dataDir = Deno.env.get("DATA_DIR") ?? dataLocalDir;

  return dataDir;
}

export function getCacheDir(): string {
  const { cacheDir } = initDirs();
  return Deno.env.get("CACHE_DIR") ?? cacheDir;
}

async function getConfigFilePath(): Promise<string> {
  const configDir = getConfigDir();

  if (configDir == null) {
    throw new Error("Config directory not defined");
  }

  const yamlFile = configDir + "/config.yaml";
  const ymlFile = configDir + "/config.yml";

  await writeConfigFileIfAbsent(ymlFile);

  return new Promise((resolve, reject) => {
    try {
      if (Deno.statSync(yamlFile).isFile) {
        return resolve(yamlFile);
      }
    } catch (_e) {
      try {
        if (Deno.statSync(ymlFile).isFile) {
          return resolve(ymlFile);
        }
      } catch (e) {
        return reject(e);
      }
    }
  });
}

/**
 * @throws {Error} when config file does not exist
 */
async function _readConfigFile(): Promise<Config> {
  const configFile = await getConfigFilePath();

  const decoder = new TextDecoder("utf-8");

  const configData = await Deno.readFile(configFile);
  const decodedFile = YAML.parse(decoder.decode(configData)) as Config;

  const finalConfig = deepmerge(defaultConfig, decodedFile);

  return finalConfig;
}

function readConfigFileMemoized() {
  let config: Config | null = null;
  return async function () {
    if (config == null) {
      config = await _readConfigFile();
      return config;
    }
    return config;
  };
}

export const readConfigFile: () => Promise<Config> = readConfigFileMemoized();

function readEnvMemoized() {
  let envFile: Record<string, string> | null = null;
  return async function (key: string) {
    const envVar = Deno.env.get(key);
    if (envVar != null) {
      return envVar;
    }
    if (envFile == null) {
      const config = await readConfigFile();
      if (config?.env?.envFile == null) {
        return null;
      }
      envFile = await dotenv.load({
        envPath: config.env.envFile,
      });
    }
    return envFile?.[key];
  };
}

export const readEnv: (key: string) => Promise<string | null> =
  readEnvMemoized();

export async function writeConfigFileIfAbsent(
  configFile: string,
): Promise<void> {
  try {
    Deno.statSync(configFile);
  } catch (e) {
    if ((e["message"] = "NotFound")) {
      const encoder = new TextEncoder();

      await fs.ensureFile(configFile);

      const file = await Deno.create(configFile);
      await file.write(encoder.encode(YAML.stringify(defaultConfig)));
    }
  }
}
