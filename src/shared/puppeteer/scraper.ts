/// <reference types="../../types/types.d.ts" />
/// @deno-types="npm:@types/puppeteer-core@latest"
import puppeteer, { Browser, Page } from "puppeteer_core";
import { stealth } from "shared/puppeteer/stealth.min.js";
import { resolveHostname } from "../utils.ts";

export class BrowserNotInitialized extends Error {
  constructor() {
    super("Browser instance not initialized!");
  }
}

export type LaunchStrategy = {
  type: "launch";
  payload: {
    path: string;
  };
};

export type RemoteStrategy = {
  type: "remote";
  payload: {
    host: string;
    port: string;
  };
};

export type PuppeteerConnectionStrategy = LaunchStrategy | RemoteStrategy;

export class PuppeteerInstance {
  _browser?: Browser;
  _strategy?: PuppeteerConnectionStrategy;

  get browser(): Browser | undefined {
    return this._browser;
  }

  get strategy(): PuppeteerConnectionStrategy {
    if (this._strategy != null) {
      return this._strategy;
    }
    throw new Error("Puppeteer connection strategy not initialized!");
  }

  async init() {
    const remoteDebuggingPort = Deno.env.get("PUPPETEER_REMOTE_PORT");
    const remoteDebuggingHost = Deno.env.get("PUPPETEER_REMOTE_HOST");
    if (remoteDebuggingPort != null) {
      const resolvedHostname = await resolveHostname(
        remoteDebuggingHost ?? "127.0.0.1",
      );
      this._strategy = {
        type: "remote",
        payload: {
          host: resolvedHostname,
          port: remoteDebuggingPort,
        },
      };
    } else {
      this._strategy = {
        type: "launch",
        payload: {
          path: Deno.env.get("PUPPETEER_BROWSER_PATH") ?? "/bin/chromium",
        },
      };
    }

    if (this.strategy.type === "remote") {
      this._browser = await puppeteer.connect({
        browserURL: "http://" + (this.strategy.payload.host ?? "127.0.0.1") +
          ":" +
          this.strategy.payload.port,
      });
    } else {
      this._browser = await puppeteer.launch({
        executablePath: this.strategy.payload.path,
      });
    }
  }

  async newPage<T>(
    handler: (page: Page) => T | Promise<T>,
  ): Promise<T> {
    if (this._browser == null) {
      throw new BrowserNotInitialized();
    }

    const page = await this._browser.newPage();
    await stealth(page);

    try {
      return await handler(page);
    } catch (e) {
      console.error(e);
      throw e;
    } finally {
      await page.close();
    }
  }

  close(): PromiseOr<void> {
    if (this._browser == null) {
      throw new BrowserNotInitialized();
    }

    if (this.strategy.type === "remote") {
      return this._browser.disconnect();
    } else {
      return this._browser.close();
    }
  }
}
