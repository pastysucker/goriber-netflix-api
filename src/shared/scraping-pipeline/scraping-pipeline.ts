import { Scraper, ScraperArgs } from "models/scraper.ts";
import { MediaSource } from "models/media/common.ts";
import { createHttpError } from 'oak/deps.ts'
import log from "logger";
import { readConfigFile } from "../config.ts";

/**
 * Runs several sources in parallel and returns their result.
 * @param scrapers - List of scrapers to run in the pipeline.
 * @param scraperArgs - Arguments passed to the individual scrapers.
 * @param shortCircuit - Don't return after first scraper successfully finished.
 */
export async function scrapingPipeline(
  scrapers: Scraper[],
  scraperArgs: ScraperArgs,
  shortCircuit = true
): Promise<MediaSource[]> {
  let all: MediaSource[] = [];
  let error: string | null = null;

  const config = await readConfigFile();
  const enabledScrapers = config.scrapers.enabled;

  for (const scraper of scrapers) {
    if (
      scraperArgs.isNsfw !== scraper.nsfw ||
      !enabledScrapers.includes(scraper.name)
    ) {
      log.info("Skipping " + scraper.name);
      continue;
    }
    log.info("Trying scraper " + scraper.name);
    try {
      const sources = await scraper.scrapeSources(scraperArgs);
      all = all.concat(sources);
      if (shortCircuit && all.length > 0) {
        break;
      }
    } catch (e) {
      if (e instanceof Error) {
        log.warn(e.message);
        if (e.name === "TypeError" || e.name === "BadGatewayError") {
          error = "One or more scrapers failed to run. Try again later.";
        }
      }
      continue;
    }
  }

  if (all.length > 0) {
    return all;
  } else {
    throw createHttpError(404, error ?? "No sources found. Nada.");
  }
}
