/// <reference types="../../types/types.d.ts" />

import { z } from "zod";
import { ExternalIds, SearchResult } from "models/media/common.ts";
import { createHttpError } from "oak/deps.ts";
import { query } from "shared/db/sqlite.ts";
import { readEnv } from "shared/config.ts";
import { fetchCached } from "shared/fetch_cached.ts";
import ItemType from "types/itemType.ts";

export const clientId = await readEnv("TRAKT_CLIENT_ID");
export const clientSecret = await readEnv("TRAKT_CLIENT_SECRET");
export const redirectUri = await readEnv("TRAKT_REDIRECT_URI");

export const tokenSchema = z.object({
  accessToken: z.string(),
  refreshToken: z.string(),
  expiresIn: z.number(),
  scope: z.string(),
  tokenType: z.string(),
  createdAt: z.number(),
});
export type TokenSchema = z.TypeOf<typeof tokenSchema>;

const baseUrl = "https://api.trakt.tv";

export function validateCredentials() {
  return clientId != null && clientSecret != null && redirectUri != null;
}

const headers = (accessToken: string) => ({
  "Content-Type": "application/json",
  "trakt-api-version": "2",
  "trakt-api-key": clientId ?? "",
  Authorization: `Bearer ${accessToken}`,
});
const buildMediaBodyPlural = (ids: ExternalIds, itemType: ItemType) => {
  const mediaItem = { ids, watched_at: new Date().toISOString() };
  const requestBody =
    itemType === ItemType.Movie
      ? { movies: [mediaItem] }
      : itemType === ItemType.Series
        ? { shows: [mediaItem] }
        : { episodes: [mediaItem] };
  return requestBody;
};
const buildMediaBody = (ids: ExternalIds, itemType: ItemType) => {
  const mediaItem = { ids, watched_at: new Date().toISOString() };
  const requestBody =
    itemType === ItemType.Movie
      ? { movie: mediaItem }
      : itemType === ItemType.Series
        ? { show: mediaItem }
        : { episode: mediaItem };
  return requestBody;
};

const fetcher = async (
  pathName: string,
  options: {
    userId?: number;
    method?: string;
    body?: Record<string, dynamic>;
    headers?: Record<string, dynamic>;
  },
): Promise<dynamic> => {
  const request = new Request(baseUrl + pathName, {
    headers:
      options.userId != null
        ? headers(await retrieveAccessToken(options.userId))
        : options.headers,
    method: options.method,
    body: options.body != null ? JSON.stringify(options.body) : undefined,
  });

  const res = await fetchCached(request, {
    addCustomCacheHeaders: true,
    maxAge: 2700,
  });

  if (res.body != null) {
    return await res.json();
  }
};

export const /* recursive */ retrieveAccessToken = async (
    userId: number,
  ): Promise<string> => {
    const [[accessToken, refreshToken, createdAt]] = query<
      [string, string, number]
    >`SELECT access_token, refresh_token, created_at FROM trakt WHERE user_id = ${userId}`;
    if (accessToken == null) {
      throw createHttpError(400, "Not logged in to Trakt!");
    } else {
      const createdDate = new Date(createdAt * 1000);

      if (new Date().getTime() - createdDate.getTime() >= 7.884e9) {
        const token = await getToken(refreshToken, "refresh_token");
        storeAccessToken(userId, token);
        return await retrieveAccessToken(userId);
      } else {
        return accessToken;
      }
    }
  };

export function storeAccessToken(userId: number, token: TokenSchema) {
  const { accessToken, refreshToken, expiresIn, scope, tokenType, createdAt } =
    token;

  query`INSERT INTO trakt (
          user_id,
          access_token,
          refresh_token,
          expires_in,
          scope,
          token_type,
          created_at
        ) values (
          ${userId},
          ${accessToken},
          ${refreshToken},
          ${expiresIn},
          ${scope},
          ${tokenType},
          ${createdAt}
        )`;
}

export function deleteAccessToken(userId: number) {
  query`DELETE FROM trakt WHERE user_id = ${userId}`;
}

// TODO: maybe provide state object for verification
export async function getToken(
  code: string,
  grantType: "authorization_code" | "refresh_token",
): Promise<TokenSchema> {
  const rest = {
    client_id: clientId,
    client_secret: clientSecret,
    redirect_uri: redirectUri,
    grant_type: grantType,
  };
  const json = await fetcher("/oauth/token", {
    body:
      grantType === "authorization_code"
        ? { code: code, ...rest }
        : { refresh_token: code, ...rest },
    headers: {
      "Content-Type": "application/json",
      "trakt-api-version": "2",
    },
    method: "POST",
  });

  const token: TokenSchema = {
    accessToken: json["access_token"],
    refreshToken: json["refresh_token"],
    scope: json["scope"],
    createdAt: json["created_at"],
    expiresIn: json["expires_in"],
    tokenType: json["token_type"],
  };

  return token;
}

export async function addToHistory(
  userId: number,
  ids: ExternalIds,
  type: ItemType,
): Promise<void> {
  const json = await fetcher("/sync/history", {
    userId,
    method: "POST",
    body: buildMediaBodyPlural(ids, type),
  });
  if (json["added"]["movies"] !== 0 || json["added"]["episodes"] !== 0) {
    return;
  } else {
    throw createHttpError(404, "Id not found in Trakt database");
  }
}

export async function removeFromHistory(
  userId: number,
  ids: ExternalIds,
  itemType: ItemType,
) {
  const json = await fetcher("/sync/history/remove", {
    userId,
    method: "POST",
    body: buildMediaBodyPlural(ids, itemType),
  });
  if (json["not_found"] != null) {
    throw createHttpError(404, "Id not found in Trakt database");
  }
}

export async function checkIn(
  userId: number,
  ids: ExternalIds,
  itemType: ItemType,
) {
  const json = await fetcher("/checkin", {
    userId,
    method: "POST",
    body: buildMediaBody(ids, itemType),
  });

  if (json["not_found"] != null) {
    throw createHttpError(404, "Id not found in Trakt database");
  }
}

export async function getWatchlist(userId: number, itemType: ItemType) {
  const json: dynamic[] = await fetcher(
    `/sync/watchlist/${itemType === ItemType.Movie ? "movies" : "shows"}/added`,
    { userId },
  );
  const traktType = itemType === ItemType.Movie ? "movie" : "show";
  return json.map<SearchResult>((v) => {
    const item = v[traktType];
    return {
      id: String(item["ids"]["tmdb"]),
      name: item["title"],
      isMovie: v["type"] === "movie",
      year: item["year"],
      adult: false,
    };
  });
}
