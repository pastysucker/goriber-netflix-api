import { sqliteCache } from "shared/db/cache.ts";

export async function fetchCached(
  request: Request,
  opts: { addCustomCacheHeaders?: boolean; maxAge?: number } = {
    addCustomCacheHeaders: false,
    maxAge: 21600,
  },
): Promise<Response> {
  const cache = sqliteCache(16000);

  const cachedResp = await cache.match(request);
  if (cachedResp == null) {
    const response = await fetch(request);
    const clonedRes = response.clone();

    if (opts.addCustomCacheHeaders) {
      const mutableHeaders = new Map(clonedRes.headers.entries());
      mutableHeaders.set("cache-control", `public, max-age=${opts.maxAge}`);
      mutableHeaders.delete("pragma");
      mutableHeaders.delete("expires");
      await cache.put(
        request,
        new Response(clonedRes.body, {
          headers: Object.fromEntries(mutableHeaders),
          status: clonedRes.status,
          statusText: clonedRes.statusText,
        }),
      );
      return response;
    }

    await cache.put(request, response);
    return response;
  } else {
    return cachedResp;
  }
}
