import { Element, Node, NodeType } from "deno_dom/deno-dom-wasm-noinit.ts";
import { isRouterContext } from "oak/utils/type_guards.ts";
import { Context } from "oak/mod.ts";
import { RouterContext } from "oak/router.ts";

export function isFile(str: string): boolean {
  const regex = new RegExp(/\.[a-z0-9]{2,4}$/i);
  const yearMatch = new RegExp(/\d{4}$/i);
  return regex.test(str) && !yearMatch.test(str);
}

/**
 * Convert `1 KB` into `1000`
 */
export function kbToBytes(str: string): number {
  return Number(str.slice(0, -3)) * 1000;
}

/**
 * Function normalizing the given variable into a proper array sequence.
 *
 * @return The resulting sequence.
 */
export function seq(
  /** The variable to normalize */
  target: string,
) {
  return typeof target === "string" ? target.split("") : target;
}

/**
 * Function squeezing the given string sequence by dropping consecutive duplicates.
 *
 * @return The resulting sequence.
 */
export function squeeze(
  /** The sequence to squeeze */ target: string,
): string {
  const sequence = seq(target),
    squeezed = [sequence[0]];

  for (let i = 1, l = sequence.length; i < l; i++) {
    if (sequence[i] !== sequence[i - 1]) {
      squeezed.push(sequence[i]);
    }
  }

  return squeezed.join("");
}

const stopwords = [
  "&",
  "and",
  "marvel's",
  "dc's",
  "-",
];

export function removeStopwords(string: string) {
  for (let index = 0; index < stopwords.length; index++) {
    string = string.toLowerCase().replaceAll(stopwords[index], " ").trim();
  }
  return string;
}

export async function arrayFromIterable<T>(
  gen: AsyncIterable<T>,
): Promise<T[]> {
  const out: T[] = [];
  for await (const x of gen) {
    out.push(x);
  }
  return out;
}

export function interpolateYears(
  start: number,
  end: number,
): [start: number, end: number] {
  // 0, 1949
  // 1950, 69
  // 1990, 99
  // 2000, 9
  if (start >= 1900 && start < 2000) {
    if (end >= 0 && end <= 100) {
      end = 1900 + end;
    }
  } else if (start >= 0 && start < 100) {
    if (end >= 1900 && end <= 2000) {
      start = 1900 + start;
    }
  } else if (start >= 2000 && start < 2100) {
    if (end >= 0 && end <= 100) {
      end = 2000 + end;
    }
  }
  return [start, end];
}

export function isYearInRange(year: number, start: number, end: number) {
  const [a, b] = interpolateYears(start, end);
  return year >= a && year <= b;
}

export function noop(): never {
  throw new Error("Not implemented");
}

export async function resolveHostname(hostname: string): Promise<string> {
  if (isNaN(parseInt(hostname)) && hostname != "localhost") {
    return (await Deno.resolveDns(hostname, "A"))[0];
  } else {
    return hostname;
  }
}

export function nodeIsElement(node: Node): node is Element {
  return node.nodeType === NodeType.ELEMENT_NODE;
}


interface GetQueryOptionsBase {
  /** The return value should be a `Map` instead of a record object. */
  asMap?: boolean;

  /** Merge in the context's `.params`.  This only works when a `RouterContext`
   * is passed. */
  mergeParams?: boolean;
}

interface GetQueryOptionsAsMap extends GetQueryOptionsBase {
  /** The return value should be a `Map` instead of a record object. */
  asMap: true;
}

/** Options which can be specified when using {@linkcode getQuery}. */
export type GetParamsOptions = GetQueryOptionsBase | GetQueryOptionsAsMap;

/** Given a context, return the `.request.url.searchParams` as a `Map` of keys
 * and values of the params. */
export function getQuery(
  ctx: Context | RouterContext<string>,
  options: GetQueryOptionsAsMap,
): Map<string, string>;
/** Given a context, return the `.request.url.searchParams` as a record object
 * of keys and values of the params. */
export function getQuery(
  ctx: Context | RouterContext<string>,
  options?: GetQueryOptionsBase,
): Record<string, string>;
export function getQuery(
  ctx: Context | RouterContext<string>,
  { mergeParams, asMap }: GetParamsOptions = {},
): Map<string, string> | Record<string, string> {
  const result: Record<string, string> = {};
  if (mergeParams && isRouterContext(ctx)) {
    Object.assign(result, ctx.params);
  }
  for (const [key, value] of ctx.request.url.searchParams) {
    result[key] = value;
  }
  return asMap ? new Map(Object.entries(result)) : result;
}
