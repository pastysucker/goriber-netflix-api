FROM denoland/deno:alpine

ENV USER="deno"

EXPOSE 6767
VOLUME /data /config

WORKDIR /app

COPY deps.ts import_map.json deno.json ./
RUN deno --version && deno cache --reload --allow-import ./deps.ts

ENV DATA_DIR="/data"
ENV CONFIG_DIR="/config"

RUN mkdir -p /data
RUN mkdir -p /config

RUN chown -R $USER /data
RUN chown -R $USER /config
RUN chown -R $USER /deno-dir

ADD . .

USER $USER

RUN deno cache --reload --allow-import ./src/server/main.ts

CMD ["run", "--allow-all", "--config=deno.json", "src/server/main.ts"]
